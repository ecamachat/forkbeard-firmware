#ifndef _OS_H_
#define _OS_H_

#include <stdlib.h>
#include <string.h>

#define os_malloc(len)              malloc((len))
#define os_memset(buf, val, len)    memset((buf), (val), (len))
#define os_free(buf)                free((buf))

#endif
