
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/select.h>
#include <sys/time.h>

#include "base64.h"
#include "fnv1a.h"

#define SHELL_CTS_STR               "READY"
#define SHELL_FAIL_STR              "FAIL:"
#define SHELL_LOG_STR               "LOG: "
#define SHELL_CTS_WAIT_MS           1000
#define BASE64_BYTES_PER_LINE       42      // Max: 54

extern int optind;
extern char *optarg;

static bool _wait_data(unsigned timeout_ms) {
  fd_set rfds;
  struct timeval tv;

  tv.tv_sec = timeout_ms / 1000;
  tv.tv_usec = (timeout_ms % 1000) * 1000;

  FD_ZERO(&rfds);
  FD_SET(STDIN_FILENO, &rfds);
  if (select(STDIN_FILENO + 1, &rfds, NULL, NULL, &tv) > 0) {
    return true;
  }
  return false;
}

#define SEND(fmt, ...)       do { \
                                LOG("<<<<< " fmt, ##__VA_ARGS__); \
                                dprintf(STDIN_FILENO, fmt, ##__VA_ARGS__); \
                                if (!_dump) { \
                                    char *line = NULL; \
                                    size_t linecap = 0; \
                                    do { \
                                        /* \
                                        if (!_wait_data(SHELL_CTS_WAIT_MS)) { \
                                            LOG("!!!!! TIMEOUT !!!!!\n"); \
                                            break; \
                                        } \
                                        */ \
                                        getline(&line, &linecap, stdin); \
                                        if (0 == strncmp(SHELL_FAIL_STR, line, strlen(SHELL_FAIL_STR))) { \
                                            LOG(">>>>> !!!!! ABORT !!!!!\n"); \
                                            abort(); \
                                        } else { \
                                            LOG(">>>>> %s", line); \
                                        } \
                                    } while (strncmp(SHELL_CTS_STR, line, strlen(SHELL_CTS_STR)));\
                                    if (line) { \
                                        free(line); \
                                    } \
                                } \
                            } while(0)

#define LOG_INIT(fname)     do { \
                                if (strlen((fname))) { \
                                    _logf = fopen((fname),  "a"); \
                                } else { \
                                    _logf = NULL; \
                                } \
                            } while(0)
#define LOG(...)            do { \
                                if (_logf) { \
                                    struct timeval tv; \
                                    struct tm now; \
                                    gettimeofday(&tv, NULL); \
                                    localtime_r(&tv.tv_sec, &now); \
                                    fprintf(_logf, "%04d-%02d-%02d %02d:%02d:%02d.%06d ", now.tm_year + 1900, now.tm_mon + 1, now.tm_mday, now.tm_hour, now.tm_min, now.tm_sec, tv.tv_usec); \
                                    fprintf(_logf, __VA_ARGS__); \
                                    fflush(_logf); \
                                } \
                            } while(0)
#define LOG_END()           do { \
                                if (_logf) { \
                                    fflush(_logf); \
                                    fclose(_logf); \
                                } \
                            } while(0)

static bool _dump = false;
static FILE *_logf = NULL;
int main(int argc, char **argv) {

    // Command line options
    int ch;
    while ((ch = getopt(argc, argv, "dl:")) != -1) {
        switch(ch) {
        case 'd':
            _dump = true;
            break;
        case 'l':
            LOG_INIT(optarg);
            break;
        default:
            fprintf(stderr, "\nfwupload [-d] [-l logfile] firmware.bin\n");
        }
    }
    argc -= optind;
    argv += optind;

    if (argc < 1) {
        LOG("No firmware file specified!\n");
        return 1;
    }

    LOG("firmware file: %s\n", argv[0]);

    unsigned char buf[80];
    FILE *fp = fopen(argv[0], "rb");
    if (fp == NULL) {
        LOG("failed to open file: %s\n", argv[0]);
        fprintf(stderr, "failed to open file: %s\n", argv[0]);
        return 1;
    }

    // Calculate checksum and size of firmware
    struct fnv1a_context fnv1a_ctx;
    size_t fsize = 0, nb;
    fnv1a_init(&fnv1a_ctx);
    while ((nb = fread(buf, 1, sizeof(buf), fp)) > 0) {
        fnv1a_update(&fnv1a_ctx, buf, nb);
        fsize += nb;
    }

    // Delay start
    // LOG("delay start\n");
    // sleep(1);

    // Post fw upload command
    // SEND("fw upload\n");
    // sleep(1);

    // Post header
    LOG("start header vvvvvvvvvvvvvvvvvvvvvvvvvvv\n");
    SEND("length: %zu\n", fsize);
    SEND("chksum: %08X\n", fnv1a_ctx.hash);
    SEND("\n");

    // Post content
    LOG("start content --------------------------\n");
    fseek(fp, 0, SEEK_SET);
    while ((nb = fread(buf, 1, BASE64_BYTES_PER_LINE, fp)) > 0) {
        unsigned char *str = base64_encode(buf, nb, NULL);
        if (str) {
            // char *cr = strchr((const char *)str, '\r');
            // char *lf = strchr((const char *)str, '\n');
            // if (cr) *cr = '\0';
            // if (lf) *lf = '\0';
            // SEND("%s\n", (const char *)str);
            SEND("%s", (const char *)str);
            free(str);
        }
    }
    SEND("\n");
    fclose(fp);
    fp = NULL;
    LOG("finished ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");

    LOG_END();
    return 0;
}
