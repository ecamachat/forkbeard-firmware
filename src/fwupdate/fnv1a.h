#ifndef _FNV1A_H_
#define _FNV1A_H_
#include <stdint.h>

struct fnv1a_context {
    uint32_t hash;
};

void fnv1a_init(struct fnv1a_context *ctx);
void fnv1a_update(struct fnv1a_context *ctx, const void* data, const uint32_t len);

#endif

