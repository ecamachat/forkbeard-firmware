/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#ifndef _FWUPDATE_H_
#define _FWUPDATE_H_

#include <stdint.h>

/* Shell input functions */
#include <shell/shell.h>
char shell_getchar(const struct shell *shell);
char* shell_getline(const struct shell *shell, char *buf, const size_t len);

#endif
