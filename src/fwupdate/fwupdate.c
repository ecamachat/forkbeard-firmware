/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <stddef.h>
#include <stdlib.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <sys/base64.h>
#include <logging/log.h>
#include <shell/shell.h>
#include <fs/fs.h>

LOG_MODULE_REGISTER(fw);

#include "fnv1a.h"
#include "fwupdate.h"

#define MAX_FIRMWARE_FILE_SIZE          (1*1024*1024)
#define SHELL_CTS(shell)                shell_print((shell), "READY")
#define SHELL_FAIL(shell, fmt, ...)     shell_print((shell), "FAIL: " fmt, ##__VA_ARGS__)
#define SHELL_RLOG(shell, fmt, ...)     shell_print((shell), "LOG: " fmt, ##__VA_ARGS__)

#define FW_FILE_NAME                    "/lfs/fw.bin"

/*
 *  Shell input functions
 */
char shell_getchar(const struct shell *shell) {
    char c;
    size_t cnt;

    shell->iface->api->read(shell->iface, &c, sizeof(c), &cnt);
    while (cnt == 0) {    
        k_busy_wait(45);
        shell->iface->api->read(shell->iface, &c, sizeof(c), &cnt);
    }
    // shell->iface->api->write(shell->iface, &c, sizeof(c), &cnt);
    return c;
}

char* shell_getline(const struct shell *shell, char *buf, const size_t len) {
    if (!buf)
        return NULL;
    
    memset(buf, 0, len);

    char *p = buf;
    for (size_t i = 0; i < (len - 1); i++) {
        char c;
        size_t cnt;

        shell->iface->api->read(shell->iface, &c, sizeof(c), &cnt);
        while (cnt == 0) {    
            k_busy_wait(45);
            shell->iface->api->read(shell->iface, &c, sizeof(c), &cnt);
        }
        // shell->iface->api->write(shell->iface, &c, sizeof(c), &cnt);
        if (c == '\n' || c == '\r') {
            break;  // end of line
        }
        *p++ = c;
    }

    return buf;
}

struct fw_upload_info {
    size_t length;
    uint32_t chksum;
};

static int _shell_fw_upload_header(const struct shell *shell, struct fw_upload_info *info) {
    if (info == NULL)
        return -1;

    memset(info, 0, sizeof(struct fw_upload_info));

    bool is_started = false;

    while (1) {
        char buf[80];
        char *s = shell_getline(shell, buf, sizeof(buf));
        size_t n = strlen(s);
        if (0 == n) {
            if (is_started) {
                SHELL_RLOG(shell, "End of header");
                SHELL_CTS(shell);
                break;
            }
            SHELL_RLOG(shell, "Skip starting blank line(s)");
            SHELL_CTS(shell);
            continue;
        }
        is_started = true;

        if (n > 8 && 0 == strncmp("length: ", s, 8)) {
            info->length = strtoul(s + 8, NULL, 10);
            if (info->length > MAX_FIRMWARE_FILE_SIZE) {
                SHELL_FAIL(shell, "Length is larger than MAX_FIRMWARE_FILE_SIZE");
                return 1;
            }
            SHELL_RLOG(shell, "Got length: %u", info->length);
        }
        else if (n > 8 && 0 == strncmp("chksum: ", s, 8)) {
            if (n != 16) {
                SHELL_FAIL(shell, "Invalid checksum format");
                return 1;
            }
            info->chksum = strtoul(s + 8, NULL, 16);
            SHELL_RLOG(shell, "Got chksum %08X", info->chksum);
        } else {
            SHELL_RLOG(shell, "Skip unknown header: %s", buf);
        }
        SHELL_CTS(shell);
    }

    if (info->length == 0) {
        SHELL_FAIL(shell, "Length is zero");
        return 1;
    }
    if (info->chksum == 0) {
        SHELL_FAIL(shell, "Missing chksum");
        return 1;
    }

    return 0;
}

static int _shell_fw_upload_content(const struct shell *shell, const struct fw_upload_info *info) {
    int err = 0;
    struct fs_file_t zfp;
    struct fs_dirent zdp;

    // Remote old file
    memset(&zdp, 0, sizeof(struct fs_dirent));
    if (0 == fs_stat(FW_FILE_NAME, &zdp)) {
        fs_unlink(FW_FILE_NAME);
    }

    // Create new file
    memset(&zfp, 0, sizeof(struct fs_file_t));
    if (0 != fs_open(&zfp, FW_FILE_NAME, FS_O_CREATE|FS_O_RDWR)) {
        memset(&zfp, 0, sizeof(struct fs_file_t));
        return -1;
    }

    // Receive from shell and write into file
    size_t total = 0;
    bool is_started = false;

    struct fnv1a_context fnv1a_ctx;
    fnv1a_init(&fnv1a_ctx);
    while (total <= info->length) {
        char s[80];
        shell_getline(shell, s, sizeof(s));
        size_t n = strlen(s);
        if (0 == n) {
            if (is_started) {
                SHELL_RLOG(shell, "End of content");
                SHELL_CTS(shell);
                break;
            }
            SHELL_RLOG(shell, "Skip starting blank line(s)");
            SHELL_CTS(shell);
            continue;
        }
        is_started = true;

        size_t len;
        if (0 != base64_decode(s, sizeof(s), &len, s, n)) {
            SHELL_FAIL(shell, "Invalid base64 text");
            err = 1;
            goto on_error;
        }
        fnv1a_update(&fnv1a_ctx, s, len);
        fs_write(&zfp, s, len);
        total += len;
        SHELL_CTS(shell);
    }

    if (info->chksum != fnv1a_ctx.hash) {
        SHELL_FAIL(shell, "Checksum mismatch: %08X -> %08X", info->chksum, fnv1a_ctx.hash);
        err = 2;
        goto on_error;
    }

    fs_sync(&zfp);
    return err;

on_error:
    if (zfp.mp) {
        fs_close(&zfp);
    }
    fs_unlink(FW_FILE_NAME);
    return err;
}


static int _cmd_upload(const struct shell *shell, size_t argc, char **argv) {
    (void)argc;
    (void)argv;

    struct fw_upload_info info;

    // Read header
    SHELL_CTS(shell);
    if (0 == _shell_fw_upload_header(shell, &info)) {
        // Read content
        _shell_fw_upload_content(shell, &info);
    }

    return 0;
}

#include <storage/flash_map.h>
#define FLASE_SECTOR_SIZE_MIN   16
static int _cmd_app(const struct shell *shell, size_t argc, char **argv) {
    (void)argc;
    (void)argv;

    int rc = 0;
    struct fs_file_t zfp;
    struct fs_dirent zdp;

    // firmware file
    memset(&zdp, 0, sizeof(struct fs_dirent));
    if (fs_stat(FW_FILE_NAME, &zdp)) {
        shell_print(shell, "unable to access file %s", FW_FILE_NAME);
    }
    shell_print(shell, "firmware file size: %zu bytes", zdp.size);
    if (zdp.size == 0) {
        goto _cmd_app_exit_normal;
    }


    memset(&zfp, 0, sizeof(struct fs_file_t));
    if (0 != fs_open(&zfp, FW_FILE_NAME, FS_O_READ)) {
        memset(&zfp, 0, sizeof(struct fs_file_t));
        goto _cmd_app_exit_normal;
    }
    shell_print(shell, "Loading from file: %s", FW_FILE_NAME);

    // get area
    const struct flash_area *fa;
    if (flash_area_open(PM_MCUBOOT_SECONDARY_ID, &fa)) {
        shell_print(shell, "failed to open flash id: %d", FLASH_AREA_ID( mcuboot_secondary));
        goto _cmd_app_exit_fw_file;   
    }
    shell_print(shell, "FlashArea: %zu KB @ %s", fa->fa_size / 1024, fa->fa_dev_name);

    // prepare block buffer
    const uint32_t blocksize = flash_area_align(fa) > FLASE_SECTOR_SIZE_MIN ? flash_area_align(fa) : FLASE_SECTOR_SIZE_MIN;
    uint8_t *blockbuff = malloc(blocksize);
    if (blockbuff == NULL) {
        shell_print(shell, "failed to alloc %" PRIu32 "bytes buffer", blocksize);
        goto _cmd_app_exit_flash;   
    }
    memset(blockbuff, flash_area_erased_val(fa), blocksize);
    shell_print(shell, "Block size %" PRIu32 " bytes", blocksize);

    // erase area
    shell_print(shell, "Erasing flash area ...");
    if (flash_area_erase(fa, 0, PM_MCUBOOT_SECONDARY_SIZE)) {
        shell_print(shell, "failed to erase flash");
        goto _cmd_app_exit_blockbuff;   
    }
    shell_print(shell, "Erasing flash area ... done");

    // write area
    shell_print(shell, "Writing flash area ...");
    off_t off = 0;
    ssize_t nb = 0;
    while (off < zdp.size) {
        nb = fs_read(&zfp, blockbuff, blocksize);
        if (nb == 0) {
            shell_print(shell, "failed to read file @ 0x%08X, err: %d", off, nb);
            break;
        } else {
            shell_fprintf(shell, SHELL_NORMAL, ".");
        }
        if (nb != blocksize) {
            nb = ((nb + (flash_area_align(fa) - 1)) / flash_area_align(fa)) * flash_area_align(fa);
        }
        if (0 != (rc = flash_area_write(fa, off, blockbuff, nb))) {
            shell_print(shell, "failed to write flash: %u @ 0x%08X, err: %d", nb, off, rc);
            break;
        }
        off += nb;
        memset(blockbuff, flash_area_erased_val(fa), blocksize);
    }
    if (nb > 0 && rc == 0) {
        shell_print(shell, "\nWriting flash area ... done");
    }

_cmd_app_exit_blockbuff:
    free(blockbuff);
_cmd_app_exit_flash:
    flash_area_close(fa);
_cmd_app_exit_fw_file:
    if (zfp.mp)
        fs_close(&zfp);
_cmd_app_exit_normal:
    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(fw_cmd,
    SHELL_CMD(app, NULL, "Upload firmware file from console", _cmd_app),
    SHELL_CMD(upload, NULL, "Upload firmware file from console", _cmd_upload),
    SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(fw, &fw_cmd, "Firmware management", NULL);
