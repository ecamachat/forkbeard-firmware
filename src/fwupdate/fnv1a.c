#include <stdint.h>
#include "fnv1a.h"

#define FNV_PRIME             16777619
#define FNV_OFFSET_BASIS    2166136261


void fnv1a_init(struct fnv1a_context *ctx) {
    ctx->hash = FNV_OFFSET_BASIS;
}

void fnv1a_update(struct fnv1a_context *ctx, const void* data, const uint32_t len) {
    const uint8_t *p = (const uint8_t*)data;
    for (uint32_t i = 0; i < len; i++) {
        ctx->hash ^= (uint32_t)*p++;
        ctx->hash = ctx->hash * FNV_PRIME;
    }
}
