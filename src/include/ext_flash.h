/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/


#ifndef MOVANO_INCLUDE_EXT_FLASH_H_
#define MOVANO_INCLUDE_EXT_FLASH_H_

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Initialize the external flash storage/filesystem
 *
 * This function sets up access to external flash storage
 *
 * @retval 0 If the operation was successful.
 *           Otherwise, a (negative) error code is returned.
 */
int ext_flash_init(void);

#ifdef __cplusplus
}
#endif
/**
 * @}
 */

#endif /* MOVANO_INCLUDE_EXT_FLASH_H_ */