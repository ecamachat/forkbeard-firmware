/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#ifndef _BTCONFIG_H_
#define _BTCONFIG_H_

/**@file
 * @defgroup bluetooth Movano Demo Service API
 * @{
 * @brief API for the Movano Demo Service
 */

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Intialize the Bluetooth Stack
 *
 * This function the bluetooth stack
 *
 * @retval 0 If the operation was successful.
 *           Otherwise, a (negative) error code is returned
 */
int bt_init(void);

/** @brief Initiate Bluetooth Advertising
 *
 * This function starts Bluetooth Advertising
 *
 * @retval 0 If the operation was successful.
 *           Otherwise, a (negative) error code is returned
 */
int bt_start_advertising(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* _BTCONFIG_H_ */