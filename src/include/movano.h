/** @file
 *  @brief Movano core definitions
 */

/*
 * Copyright (c) 2021 Movano
 */

#ifndef MOVANO_H_
#define MOVANO_H_

#ifdef __cplusplus
extern "C" {
#endif

#define MOVANO_PROMPT        "MOVANO"
#define MOVANO_ID_PREFIX     "MOV"


#ifdef __cplusplus
}
#endif
/**
 * @}
 */

#endif /* MOVANO_H_ */
