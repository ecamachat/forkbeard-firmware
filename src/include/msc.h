/****************************************************************************
 * Movano Secure Connection
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#ifndef _MSC_H_
#define _MSC_H_

#include <zephyr.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/byteorder.h>
#include <mbedtls/gcm.h>

/*
 * Movano Secure Connection Sesseion
 */
struct _msc_context_t;
typedef struct _msc_context_t msc_context_t;

int msc_session_setup(msc_context_t *ctx);
int msc_session_tx(msc_context_t *ctx, void *buf, unsigned len);
int msc_session_rx(msc_context_t *ctx, void *buf, unsigned len);
int msc_session_cleanup(msc_context_t *ctx);

#endif//_MSC_H_
