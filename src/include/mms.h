/****************************************************************************
 * Movano Message Service
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#ifndef _MMS_H_
#define _MMS_H_

#include <zephyr.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/byteorder.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/gatt.h>
#include <bluetooth/uuid.h>

#include "sec.h"

/*
 * Movano Message Service
 * 
 */
/* Movano Message Service UUID */
#define BT_UUID_MMS_SVC_VAL     BT_UUID_128_ENCODE(0x5a6eA000, 0x2486, 0x8d30, 0x2327, 0xec245248d79a)
/* Movano Message Service Rx Characteristic UUID */
#define BT_UUID_MMS_RX_VAL      BT_UUID_128_ENCODE(0x5a6eA001, 0x2486, 0x8d30, 0x2327, 0xec245248d79a)
/* Movano Message Service Tx Characteristic UUID */
#define BT_UUID_MMS_TX_VAL      BT_UUID_128_ENCODE(0x5a6eA002, 0x2486, 0x8d30, 0x2327, 0xec245248d79a)

#define BT_UUID_MMS_SVC         BT_UUID_DECLARE_128(BT_UUID_MMS_SVC_VAL)
#define BT_UUID_MMS_RX          BT_UUID_DECLARE_128(BT_UUID_MMS_RX_VAL)
#define BT_UUID_MMS_TX          BT_UUID_DECLARE_128(BT_UUID_MMS_TX_VAL)

/*
 * Movano Message Service framing
 */
typedef enum _mms_resource_type_t {
    mms_resource_type_ctrl = 0,
    mms_resource_type_data,
    mms_resource_type_stream,
    mms_resource_type_file,
    mms_resource_type_time,
    MMS_RESOURCE_TYPE_MAX,
} mms_resource_type_t;


typedef struct __attribute__ ((__packed__)) _mms_header_fragment_t {
    uint16_t seqno;
    uint8_t id;
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    uint8_t proto:3;
    uint8_t class:1;
    uint8_t detail:4;

    uint32_t length:24;
    uint32_t resource:4;
    uint32_t more:1;
    uint32_t reserved:3;
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t detail:4;
    uint8_t class:1;
    uint8_t proto:3;

    uint32_t reserved:3;
    uint32_t more:1;
    uint32_t resource:4;
    uint32_t length:24;
#else
    #error Unknown byte order!
#endif
    uint8_t  payload[];
} mms_header_fragment_t;

typedef struct __attribute__ ((__packed__)) _mms_data_fragment_t {
    uint16_t seqno;
    uint8_t  payload[];
} mms_data_fragment_t;

typedef struct __attribute__ ((__packed__)) _mms_fragment_t {
    mms_header_fragment_t header;
    mms_data_fragment_t data;
} mms_fragment_t;

/*
 * Movano Message Service payload messages
 */
typedef enum _mms_ctrl_action_t {
    mms_ctrl_action_get = 0,
    mms_ctrl_action_set = 1,
} mms_ctrl_action_t;

typedef struct __attribute__ ((__packed__)) _mms_ctrl_message_t {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    uint8_t opcode:7;
    uint8_t action:1;

    uint8_t length;
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t action:1;
    uint8_t opcode:7;

    uint8_t length;
#else
    #error Unknown byte order!
#endif
    uint8_t  payload[];
} mms_ctrl_message_t;

typedef struct __attribute__ ((__packed__)) _mms_data_message_t {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    uint8_t opcode:7;
    uint8_t is_live:1;

    uint8_t length;
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t is_live:1;
    uint8_t opcode:7;

    uint8_t length;
#else
    #error Unknown byte order!
#endif
    uint8_t  payload[];
} mms_data_message_t;

typedef struct __attribute__ ((__packed__)) _mms_stream_message_t {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    uint8_t opcode:7;
    uint8_t reserved:1;

    uint8_t length;
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t reserved:1;
    uint8_t opcode:7;

    uint8_t length;
#else
    #error Unknown byte order!
#endif
    uint8_t  payload[];
} mms_stream_message_t;

typedef struct __attribute__ ((__packed__)) _mms_file_message_t {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    uint8_t opcode:7;
    uint8_t reserved:1;

    uint8_t length;
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t reserved:1;
    uint8_t opcode:7;

    uint8_t length;
#else
    #error Unknown byte order!
#endif
    uint8_t  filename[16];
    uint8_t  payload[];
} mms_file_message_t;

typedef struct __attribute__ ((__packed__)) _mms_timesync_message_t {
    uint8_t length;
    uint8_t  payload[];
} mms_timesync_message_t;

/*
 * Movano Message Service task config
 */
#define MMS_TIMER_THREAD_PRIO   11
#define MMS_TIMER_STACK_BYTES   1024

#define USE_MMS_RX_THREAD       1
#define MMS_RX_THREAD_PRIO      12
#define MMS_RX_STACK_BYTES      4096

#define USE_MMS_TX_THREAD       1
#define MMS_TX_THREAD_PRIO      13
#define MMS_TX_STACK_BYTES      768

/*
 * Movano Message Service session states
 */
#define MMS_MAX_MESSAGE_LEN     256
enum mms_session_state_t {
    MMS_SESSION_STATE_CLOSED = 0,
    MMS_SESSION_STATE_WAITING_MSG_FRAG,
    MMS_SESSION_STATE_SENDING_RESPONSE,
    MMS_SESSION_STATE_PROCESSING,
    MMS_SESSION_STATE_SENDING_MSG,
    MMS_SESSION_STATE_WAITING_MSG_RECEIPT,
};

/*
 * Movano Message Service context
 */
typedef struct _mms_context_t {
    // Connection and status
    uint8_t index;
    struct bt_conn *conn;
    bool mms_tx_ready;
#if defined(USE_MMS_RX_THREAD) && USE_MMS_RX_THREAD > 0
    // session rx queue
    struct k_mutex rxq_lock;
    uint16_t rxq_len;
    uint8_t rxq_buf[CONFIG_BT_L2CAP_TX_MTU-3];
#endif
#if defined(USE_MMS_TX_THREAD) && USE_MMS_TX_THREAD > 0
    // session tx queue
    struct k_mutex txq_lock;
    uint16_t txq_len;
    uint8_t txq_buf[CONFIG_BT_L2CAP_TX_MTU-3];
#endif
    // Secure Message Rx session state and status
    struct k_mutex rx_session_lock;
    enum mms_session_state_t rx_session_state;
    struct k_timer rx_session_timer;
    uint8_t rx_session_buf[MMS_MAX_MESSAGE_LEN];        // Current Rx secure message buffer
    size_t rx_session_len;                              // Total number of byte of the secure message
    size_t rx_session_off;                              // Current progress of Rx
    uint16_t rx_session_idx;                            // Current index of fragment
    // Secure Message Tx session state and status
    struct k_mutex tx_session_lock;
    enum mms_session_state_t tx_session_state;
    struct k_timer tx_session_timer;
    uint8_t tx_session_buf[MMS_MAX_MESSAGE_LEN];        // Current Tx secure message buffer
    size_t tx_session_len;                              // Total number of byte of the secure message
    size_t tx_session_off;                              // Current progress of Tx
    uint16_t tx_session_idx;                            // Current index of fragment
} mms_context_t;

typedef struct _mms_service_t {
    const struct bt_gatt_service_static *mms_gatt_svc;
    const struct bt_gatt_attr *mms_tx_gatt_attr;

    uint8_t mms_timer_msgq_buf[CONFIG_BT_MAX_CONN];
    struct k_msgq mms_timer_msgq;
    struct k_thread mms_timer_thread;

#if defined(USE_MMS_RX_THREAD) && USE_MMS_RX_THREAD > 0
    uint8_t mms_ble_rx_msgq_buf[CONFIG_BT_MAX_CONN];
    struct k_msgq mms_ble_rx_msgq;
    struct k_thread mms_ble_rx_thread;
#endif

#if defined(USE_MMS_TX_THREAD) && USE_MMS_TX_THREAD > 0
    uint8_t mms_ble_tx_msgq_buf[CONFIG_BT_MAX_CONN];
    struct k_msgq mms_ble_tx_msgq;
    struct k_thread mms_ble_tx_thread;
#endif
} mms_service_t;

/*
 * Movano Message Service functions
 */
void mms_ble_init(const struct bt_gatt_service_static *svc);
void mms_ble_connected(struct bt_conn *conn, uint8_t err);
void mms_ble_disconnected(struct bt_conn *conn, uint8_t reason);
ssize_t mms_rx_handler(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
void mms_tx_ccc_cfg_changed(const struct bt_gatt_attr *attr, uint16_t value);
ssize_t mms_ble_tx(mms_context_t *ctx, const void *buf, uint16_t len);
void mms_notify_new_data(void *buf, uint16_t len);

#endif//_MMS_H_
