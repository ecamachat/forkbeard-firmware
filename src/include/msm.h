/****************************************************************************
 * Movano Secure Message
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#ifndef _MSM_H_
#define _MSM_H_

#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/byteorder.h>

#define SECURE_MESSAGE_VERSION_1            1

#define SECURE_MESSAGE_CRYPTO_AES_GCM_256   1

typedef struct msm_header_ {
    uint8_t version;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t destination:4;
    uint8_t source:4;
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    uint8_t source:4;
    uint8_t destination:4;
#else
    #error Unknown byte order
#endif
    uint8_t crypto;
    uint8_t reserved;
    uint8_t nonce[32];
    uint32_t length;
    uint8_t payload[];
} msm_header_t;

typedef struct msm_footer_ {
    uint8_t authtag[16];
} msm_footer_t;

typedef struct msm_context_ {

} msm_context_t;

/*
 * Movano Secure Message
 */
bool movano_secure_message_encrypt(uint8_t *msgbuf, size_t length);
bool movano_secure_message_decrypt(uint8_t *msgbuf, size_t length);
// TODO: Add file reader/writer for large message

#endif//_MSM_H_
