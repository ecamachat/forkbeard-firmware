#ifndef SEC_H_
#define SEC_H_

#include <stdint.h>
#include <time.h>

#include <mbedtls/ecdh.h>
#include <mbedtls/md.h>
#include <mbedtls/sha256.h>
#include <mbedtls/gcm.h>
#include <mbedtls/cipher.h>

void movano_sec_init(void);
bool movano_sha256hmac(const uint8_t *key, const uint8_t *data, const size_t datalen, uint8_t *hash);

/*
 * ECC certificates
 */
#define EC_PUBKEY_LEN   32
#define EC_PRIKEY_LEN   32
#define EC_SECRET_LEN   32
#define HMAC_KEY_LEN    32
#define HMAC_BLK_LEN    64
#define HMAC_SUM_LEN    32

#define CIPHER_KEY_LEN  32
#define CIPHER_IV_LEN   12

typedef struct __attribute__ ((__packed__)) _sec_private_key_t {
    uint8_t private_key[EC_PRIKEY_LEN];
} sec_private_key_t;

typedef struct __attribute__ ((__packed__)) _sec_public_key_t {
    uint8_t public_key[EC_PUBKEY_LEN];
} sec_public_key_t;

typedef struct __attribute__ ((__packed__)) _sec_secret_key_t {
    uint8_t secret_key[CIPHER_KEY_LEN];
} sec_secret_key_t;

typedef struct __attribute__ ((__packed__)) _sec_hmac_key_t {
    uint8_t hmac_key[HMAC_KEY_LEN];
} sec_hmac_key_t;

typedef struct __attribute__ ((__packed__)) _sec_certificate_t {
    uint8_t  version;
    uint32_t timestamp;
    uint32_t serial_no;
    uint32_t valid_from;
    uint32_t valid_thru;
    uint32_t max_usage;
    uint64_t allowed_ops;
    uint8_t  public_key[EC_PUBKEY_LEN];
} sec_certificate_t;


/*
 * Movano Secure Connection
 */

typedef struct _movano_hkdf_ctx_t {
    uint8_t prk[HMAC_SUM_LEN];
    uint32_t i;
    uint8_t t[HMAC_SUM_LEN + sizeof(uint32_t)];
    size_t t_len;
    uint8_t okm[HMAC_SUM_LEN*2];
    size_t okm_off;
    size_t okm_len;
} movano_hkdf_ctx_t;

/*
 * Movano Secure KEY/IV per frame/message
 */
void movano_hkdf_init(movano_hkdf_ctx_t *ctx, const uint8_t *salt, const uint8_t *ikm);
void movano_hkdf_next(movano_hkdf_ctx_t *ctx, const size_t length, uint8_t *output);
void movano_hkdf_free(movano_hkdf_ctx_t *ctx);

bool movano_secure_derivate_hkdf(const sec_certificate_t *cert, const uint8_t *nonce, movano_hkdf_ctx_t *hkdf_ctx);
bool movano_secure_derivate_key_iv(const sec_certificate_t *cert, const uint8_t *nonce, uint8_t *key, uint8_t *iv);

/*
 * RTC Time Sync
 */
bool movano_is_rtc_ok(void);
time_t movano_current_rtc(void);
void movano_update_rtc(time_t rtc_ts);

/*
 * Secure Certificate/Key/Counter Storage
 */
uint32_t movano_secure_message_counter_tx(bool incr);
uint32_t movano_secure_message_counter_rx(bool incr);
int movano_secure_certificate_install(sec_certificate_t *cert);
int movano_secure_certificate_revoke(uint32_t serial_no);
int movano_secure_certificate_get(uint32_t serial_no, sec_certificate_t *cert);

#endif /* SEC_H_ */
