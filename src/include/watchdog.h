/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#ifndef MOVANO_INCLUDE_WATCHDOG_H_
#define MOVANO_INCLUDE_WATCHDOG_H_

#ifdef __cplusplus
extern "C" {
#endif

#if DT_HAS_COMPAT_STATUS_OKAY(nordic_nrf_watchdog)
#define WDT_NODE DT_INST(0, nordic_nrf_watchdog)
#endif

#define WDOG_TIMEOUT                    60
#define WDOG_TIMEOUT_MS                 (WDOG_TIMEOUT * 1000U)
#define WDOG_REFRESH_INTERVAL_SECONDS   30

/** @brief Initialize the Hardware Watchdog.
 *
 * This function sets up the hardware watchdog registers, then starts
 * the watchdog.
 *
 * @retval 0 If the operation was successful.
 *           Otherwise, a (negative) error code is returned.
 */
int wdog_init(void);

#ifdef __cplusplus
}
#endif
/**
 * @}
 */

#endif /* MOVANO_INCLUDE_WATCHDOG_H_ */