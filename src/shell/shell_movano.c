/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <shell/shell.h>
#include <shell/shell_uart.h>

#include "movano.h"

extern const char *FW_VERSION;

static int _shell_movano_cmd_version(const struct shell *shell, size_t argc, char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

        shell_print(shell, "Movano version %s", FW_VERSION);

        return 0;
}
SHELL_CMD_ARG_REGISTER(version, NULL, "Show movano version", _shell_movano_cmd_version, 1, 0);

static int _shell_set_movano_prompt(void)
{
    const struct shell *shell_uart;

    shell_uart = shell_backend_uart_get_ptr();
    if (shell_uart == NULL) {
        printk("Error: unable to retrive shell pointer\n");
        return -1;
    }

    return shell_prompt_change(shell_uart, MOVANO_PROMPT"> ");
}

int shell_init_movano(void)
{
    return _shell_set_movano_prompt();
}
