/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#define USE_DEV_MODE                0

#include <zephyr.h>
#include <logging/log.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <errno.h>
#include <sys/byteorder.h>
#include <sys/base64.h>
#include <random/rand32.h>
#include <time.h>
#include <posix/time.h>
#include <sys/timeutil.h>

#include <psa/crypto.h>
#include <nrf_cc3xx_platform.h>
#include <nrf_cc3xx_platform_kmu.h>
#include <nrf_cc3xx_platform_ctr_drbg.h>
#include <hw_unique_key.h>

#include "sec.h"

#define DEFAULT_DEVICE_PRIKEY   { \
                                    0x28, 0x56, 0x7b, 0x83, 0x35, 0xd2, 0xc4, 0xb5, 0x9c, 0xc1, 0xc5, 0xe8, \
                                    0xaa, 0x85, 0xa9, 0x39, 0xc2, 0x2c, 0xd4, 0x53, 0x0d, 0xc6, 0xe6, 0x9a, \
                                    0xcc, 0x83, 0x9e, 0x51, 0x02, 0x72, 0x85, 0x6c \
                                }
#define DEFAULT_DEVICE_PUBKEY   { \
                                    0x69, 0xea, 0x3e, 0x08, 0xf5, 0x17, 0x72, 0x16, 0x1f, 0x08, 0xd7, 0xb2, \
                                    0x49, 0xa4, 0x7c, 0x20, 0xb8, 0x54, 0xf9, 0xe2, 0xf0, 0x82, 0x1f, 0x73, \
                                    0x02, 0x98, 0xd9, 0xa4, 0xfb, 0x83, 0x31, 0x32 \
                                }
#define DEFAULT_BACKEND_PUBKEY  { \
                                    0xd4, 0x5f, 0xd8, 0x6f, 0x17, 0x98, 0x57, 0xae, 0x94, 0xcd, 0x8e, 0xfe, \
                                    0x7f, 0xde, 0xbd, 0x2c, 0xc5, 0xfd, 0xc9, 0xd6, 0x30, 0x19, 0x5b, 0xe2, \
                                    0x0e, 0xaa, 0x56, 0xb1, 0x54, 0xec, 0xd3, 0x04 \
                                }

static sec_private_key_t _dk = {
    .private_key = DEFAULT_DEVICE_PRIKEY,
};

static const sec_private_key_t *_get_device_key(void);

LOG_MODULE_REGISTER(sec);

static bool deriveSharedSecret(const sec_private_key_t *privateKey, const sec_public_key_t *publicKey, sec_secret_key_t *sharedSecret) {
    int err;
    bool rc = false;

    /* Curve */
    mbedtls_ecp_group curve25519;
    mbedtls_ecp_group_init(&curve25519);
    err = mbedtls_ecp_group_load(&curve25519, MBEDTLS_ECP_DP_CURVE25519);
    if (err) {
        LOG_ERR("mbedtls_ecp_group_load failed: -0x%04X", (0-err));
        goto free_curve25519;
    }
    /* Peer public key */
    mbedtls_ecp_point hisPubKey; // public key
    mbedtls_ecp_point_init(&hisPubKey);
    err = mbedtls_ecp_point_read_binary(&curve25519, &hisPubKey, publicKey->public_key, EC_PUBKEY_LEN);
    if (err) {
        LOG_ERR("mbedtls_ecp_point_read_binary failed to load public key: -0x%04X", (0-err));
        goto free_pubkey;
    }
    /* My private key */
    mbedtls_mpi myPriKey;       // private key
    mbedtls_mpi_init(&myPriKey);
    err = mbedtls_mpi_read_binary_le(&myPriKey, privateKey->private_key, EC_PRIKEY_LEN);
    if (err) {
        goto free_prikey;
        LOG_ERR("mbedtls_mpi_read_binary failed to load private key: -0x%04X", (0-err));
    }

    mbedtls_mpi ourSecret;   // shared secret
    mbedtls_mpi_init(&ourSecret);
    err = mbedtls_ecdh_compute_shared(&curve25519, &ourSecret, &hisPubKey, &myPriKey, NULL, NULL);
    if (0 == err) {
        mbedtls_mpi_write_binary_le(&ourSecret, sharedSecret->secret_key, mbedtls_mpi_size(&ourSecret));
        rc = true;
    } else {
        LOG_ERR("mbedtls_ecdh_compute_shared failed: -0x%04X", (0-err));
    }
    mbedtls_mpi_free(&ourSecret);
free_prikey:
    mbedtls_mpi_free(&myPriKey);
free_pubkey:
    mbedtls_ecp_point_free(&hisPubKey);
free_curve25519:
    mbedtls_ecp_group_free(&curve25519);

    return rc;
}

bool movano_sha256hmac(const uint8_t *key, const uint8_t *data, const size_t datalen, uint8_t *hash) {
    int err;
    const mbedtls_md_info_t *sha256info = mbedtls_md_info_from_type(MBEDTLS_MD_SHA256);
    if (sha256info == NULL) {
        LOG_ERR("SHA256: not supported!");
        return false;
    }
    // LOG_HEXDUMP_INF(key, HMAC_KEY_LEN, "movano_sha256hmac: key:");
    // LOG_HEXDUMP_INF(data, datalen, "movano_sha256hmac: msg:");

    mbedtls_md_context_t ctx;
    mbedtls_md_init(&ctx);
    if ((err = mbedtls_md_setup(&ctx, sha256info, 1))) {
        LOG_ERR("movano_sha256hmac: mbedtls_md_setup failed: -0x%04X", (0-err));
        goto cleanup;
    }
    if ((err = mbedtls_md_hmac_starts(&ctx, key, HMAC_KEY_LEN)) != 0) {
        LOG_ERR("movano_sha256hmac: mbedtls_md_hmac_starts failed: -0x%04X", (0-err));
        goto cleanup;
    }
    if ((err = mbedtls_md_hmac_update(&ctx, data, datalen)) != 0) {
        LOG_ERR("movano_sha256hmac: mbedtls_md_hmac_update failed: -0x%04X", (0-err));
        goto cleanup;
    }
    if ((err = mbedtls_md_hmac_finish(&ctx, hash)) != 0) {
        LOG_ERR("movano_sha256hmac: mbedtls_md_hmac_finish failed: -0x%04X", (0-err));
        goto cleanup;
    }
cleanup:
    mbedtls_md_free(&ctx);

    // LOG_HEXDUMP_INF(hash, HMAC_SUM_LEN, "movano_sha256hmac: mac:");
    return err == 0;
}

/*
 * Movano Secure KEY/IV per frame/message
 */

void movano_hkdf_init(movano_hkdf_ctx_t *ctx, const uint8_t *salt, const uint8_t *ikm) {
    movano_sha256hmac(salt, ikm, HMAC_SUM_LEN, ctx->prk);
    ctx->i = 0;
    ctx->t_len = 0;
    ctx->okm_off = 0;
    ctx->okm_len = 0;
}

void movano_hkdf_next(movano_hkdf_ctx_t *ctx, const size_t length, uint8_t *output) {
    size_t fed = 0;
    size_t remain = length - fed;

    if (ctx->okm_len >= length) {
        memcpy(output, &ctx->okm[ctx->okm_off], length);
        ctx->okm_len -= length;
        ctx->okm_off += length;
        return;
    }

    memcpy(output, &ctx->okm[ctx->okm_off], ctx->okm_len);
    fed = ctx->okm_len;
    remain = length - fed;
    ctx->okm_len = 0;
    ctx->okm_off = 0;

    while (fed < length) {
        // Generate next OKM output
        ctx->i++;
        *((uint32_t*)&ctx->t[ctx->t_len]) = sys_cpu_to_be32(ctx->i);
        ctx->t_len += sizeof(uint32_t);
        // LOG_HEXDUMP_DBG(ctx->t, ctx->t_len, "T||n:");
        movano_sha256hmac(ctx->prk, ctx->t, ctx->t_len, ctx->t);
        ctx->t_len = HMAC_SUM_LEN;
        // LOG_HEXDUMP_DBG(ctx->t, ctx->t_len, "T:");
        memcpy(&ctx->okm[ctx->okm_off], ctx->t, ctx->t_len);
        ctx->okm_len += ctx->t_len;
        // Feed to output
        if (ctx->okm_len > remain) {
            memcpy(&output[fed], &ctx->okm[ctx->okm_off], remain);
            fed += remain;
            ctx->okm_len -= remain;
            ctx->okm_off += remain;
        } else {
            memcpy(&output[fed], &ctx->okm[ctx->okm_off], ctx->okm_len);
            fed += ctx->okm_len;
            ctx->okm_len = 0;
            ctx->okm_off = 0;
        }
        remain = length - fed;
    }
}

void movano_hkdf_free(movano_hkdf_ctx_t *ctx) {
    memset(ctx, 0, sizeof(movano_hkdf_ctx_t));
}

bool movano_secure_derivate_hkdf(const sec_certificate_t *cert, const uint8_t *nonce, movano_hkdf_ctx_t *hkdf_ctx) {
    // load peer's public key
    const sec_public_key_t *peer_pub = (const sec_public_key_t*)cert->public_key;
    sec_secret_key_t ecies;
    if (!deriveSharedSecret(_get_device_key(), peer_pub, &ecies)) {
        LOG_ERR("movano_secure_derivate_key_iv: ecies failed!");
        return false;
    }

    movano_hkdf_init(hkdf_ctx, nonce, ecies.secret_key);
    return true;
}

bool movano_secure_derivate_key_iv(const sec_certificate_t *cert, const uint8_t *nonce, uint8_t *key, uint8_t *iv) {
    // load peer's public key
    const sec_public_key_t *peer_pub = (const sec_public_key_t*)cert->public_key;
    sec_secret_key_t ecies;
    if (!deriveSharedSecret(_get_device_key(), peer_pub, &ecies)) {
        LOG_ERR("movano_secure_derivate_key_iv: ecies failed!");
        return false;
    }

    movano_hkdf_ctx_t hkdf_ctx;
    movano_hkdf_init(&hkdf_ctx, nonce, ecies.secret_key);
    movano_hkdf_next(&hkdf_ctx, CIPHER_KEY_LEN, key);
    movano_hkdf_next(&hkdf_ctx, CIPHER_IV_LEN, iv);
    movano_hkdf_free(&hkdf_ctx);
    return true;
}

/*
 * RTC Time Sync
 */
static time_t _last_rtc_update = 0;
bool movano_is_rtc_ok(void) {
    if (_last_rtc_update <= 0){
        LOG_DBG("_last_rtc_update <= 0");
        return false;
    }

	struct timespec tp;
	clock_gettime(CLOCK_REALTIME, &tp);
    if (tp.tv_sec > (_last_rtc_update + (time_t)604800)) {
        LOG_DBG("_last_rtc_update tv_sec (%"PRId64") < (%"PRId64")", tp.tv_sec, (_last_rtc_update + (time_t)604800));
        return false;
    }
    return true;
}

time_t movano_current_rtc(void) {
    if (_last_rtc_update > 0) {
        struct timespec tp;
        clock_gettime(CLOCK_REALTIME, &tp);
        LOG_DBG("RTC: %"PRId64, tp.tv_sec);
        return tp.tv_sec;
    }
    return 0;
}

void movano_update_rtc(time_t rtc_ts) {
    if (rtc_ts == 0) {
        _last_rtc_update = 0;
        return;
    }
    if (rtc_ts <= _last_rtc_update) {
        return;
    }

	struct timespec tp;
    tp.tv_sec = rtc_ts;
    tp.tv_nsec = 0;
    if (0 != clock_settime(CLOCK_REALTIME, &tp)) {
        LOG_ERR("RTC: could not be set");
    } else {
        _last_rtc_update = rtc_ts;
        LOG_INF("RTC: updated to %"PRId64, rtc_ts);
    }
}

/*
 * Secure Certificate/Key/Counter Storage
 */

uint32_t movano_secure_message_counter_tx(bool incr) {
    // TODO: Access a real secure storage
    return 0;
}

uint32_t movano_secure_message_counter_rx(bool incr) {
    // TODO: Access a real secure storage
    return 0;
}

int movano_secure_certificate_install(sec_certificate_t *cert) {
    // TODO: Access a real secure storage
    (void)cert;
    return 0;
}

int movano_secure_certificate_revoke(uint32_t serial_no) {
    // TODO: Access a real secure storage
    (void)serial_no;
    return 0;
}

int movano_secure_certificate_get(uint32_t serial_no, sec_certificate_t *cert) {
    // TODO: Access a real secure storage
    (void)serial_no;
    (void)cert;
    return 0;
}

static const sec_private_key_t* _get_device_key(void) {
    // TODO: Access a real secure storage
    return &_dk;
}

/*
 * Initialize Security Module
 */
void movano_sec_init(void) {
    psa_status_t status = psa_crypto_init();
    if (status != PSA_SUCCESS) {
        LOG_ERR("failed to init PSA crypto!");
    }
#ifndef HUK_HAS_KMU
    #error Needs HUK_HAS_KMU
#endif
    if (!nrf_cc3xx_platform_is_initialized()) {
        if (nrf_cc3xx_platform_init() != NRF_CC3XX_PLATFORM_SUCCESS) {
            LOG_ERR("failed to init cc3xx!");
        }
    }
    if (!nrf_cc3xx_platform_rng_is_initialized()) {
        if (nrf_cc3xx_platform_init_hmac_drbg() != NRF_CC3XX_PLATFORM_SUCCESS) {
            LOG_ERR("failed to init CC3xx RNG!");
        }
    }
    if (!(hw_unique_key_is_written(HUK_KEYSLOT_MKEK) && hw_unique_key_is_written(HUK_KEYSLOT_MEXT))) {
        LOG_WRN("Writing random keys to KMU");
        hw_unique_key_write_random();
    }

    LOG_INF("initialized");
}

#if CONFIG_SHELL
/*
 * Shell commands
 */
#include <shell/shell.h>
#include <shell/shell_uart.h>

static int _cmd_sec_test(const struct shell *shell, size_t argc, char **argv) {
    ARG_UNUSED(shell);
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(sec,
    SHELL_CMD(test, NULL, "SEC self test", _cmd_sec_test),
    SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(sec, &sec, "SEC commands", NULL);
#endif
