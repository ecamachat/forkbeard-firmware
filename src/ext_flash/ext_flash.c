/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <zephyr.h>
#include <stdio.h>
#include <device.h>
#include <fs/fs.h>
#include <fs/littlefs.h>
#include <storage/flash_map.h>
#include <hw_unique_key.h>
#include <psa/crypto_types.h>

#include "ext_flash.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(ext_flash);

/* Matches LFS_NAME_MAX */
#define MAX_PATH_LEN 255

FS_LITTLEFS_DECLARE_DEFAULT_CONFIG(lfs_data);

static struct fs_mount_t lfs_storage_mnt = {
    .type = FS_LITTLEFS,
    .fs_data = &lfs_data,
    .storage_dev = (void *)FLASH_AREA_ID(littlefs_storage),
    .mnt_point = "/lfs"
};

psa_key_id_t ext_flash_xts_key_id;
static uint8_t ext_flash_xts_keys[32];   // Two 128-bit keys
static int ext_flash_xts_bind(struct fs_littlefs *lfs);
static int ext_flash_xts_read(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, void *buffer, lfs_size_t size);
static int ext_flash_xts_prog(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, const void *buffer, lfs_size_t size);
static int ext_flash_xts_erase(const struct lfs_config *c, lfs_block_t block);

int ext_flash_init(void)
{
    struct fs_mount_t *mp = &lfs_storage_mnt;
    unsigned int id = (uintptr_t)mp->storage_dev;
    struct fs_statvfs sbuf;
    const struct flash_area *pfa;
    int rc;

    /* Get external flash informations */
    rc = flash_area_open(id, &pfa);
    if (rc < 0) {
        LOG_ERR("FAIL: unable to find flash area %u: %d\n", id, rc);
        return -1;
    }
    const struct device *dev = flash_area_get_device(pfa);
    LOG_INF("device: %s, %zu bytes, has%s driver", dev->name, pfa->fa_size, flash_area_has_driver(pfa) == 1 ? "" : " no");
    flash_area_close(pfa);

    // TODO: Fix AES-XTS layer
    uint8_t key_label[] = "Movano storage encryption key derivation";
    ext_flash_xts_key_id = hw_unique_key_derive_key(HUK_KEYSLOT_MEXT, NULL, 0, key_label, sizeof(key_label) - 1, ext_flash_xts_keys, sizeof(ext_flash_xts_keys));
    ext_flash_xts_bind(&lfs_data);
    lfs_data.cfg.read  = ext_flash_xts_read;
    lfs_data.cfg.prog  = ext_flash_xts_prog;
    lfs_data.cfg.erase = ext_flash_xts_erase;

    rc = fs_mount(mp);
    if (rc < 0) {
            LOG_ERR("FAIL: mount id %u at %s: %d\n", (unsigned int)mp->storage_dev, mp->mnt_point, rc);
            return -1;
    }
    LOG_INF("%s mount: %d\n", mp->mnt_point, rc);

    rc = fs_statvfs(mp->mnt_point, &sbuf);
    if (rc < 0) {
            LOG_ERR("FAIL: statvfs: %d\n", rc);
            fs_unmount(mp);
            return -1;
    }
    /*
     * f_bsize Optimal transfer block size
     * f_frsize Allocation unit size
     * f_blocks Size of FS in f_frsize units
     * f_bfree Number of free blocks
     */
    LOG_INF("External flash initialized: %lu%% free.", (sbuf.f_bfree * 100u) / sbuf.f_blocks);
    return 0;
}


/*
 * External flash full filesystem AES-XTS encryption
 */

static int errno_to_lfs(int error)
{
	if (error >= 0) {
		return LFS_ERR_OK;
	}

	switch (error) {
	default:
	case -EIO:              /* Error during device operation */
		return LFS_ERR_IO;
	case -EFAULT:		/* Corrupted */
		return LFS_ERR_CORRUPT;
	case -ENOENT:           /* No directory entry */
		return LFS_ERR_NOENT;
	case -EEXIST:           /* Entry already exists */
		return LFS_ERR_EXIST;
	case -ENOTDIR:          /* Entry is not a dir */
		return LFS_ERR_NOTDIR;
	case -EISDIR:           /* Entry is a dir */
		return LFS_ERR_ISDIR;
	case -ENOTEMPTY:        /* Dir is not empty */
		return LFS_ERR_NOTEMPTY;
	case -EBADF:            /* Bad file number */
		return LFS_ERR_BADF;
	case -EFBIG:            /* File too large */
		return LFS_ERR_FBIG;
	case -EINVAL:           /* Invalid parameter */
		return LFS_ERR_INVAL;
	case -ENOSPC:           /* No space left on device */
		return LFS_ERR_NOSPC;
	case -ENOMEM:           /* No more memory available */
		return LFS_ERR_NOMEM;
	}
}

static int ext_flash_xts_bind(struct fs_littlefs *lfs){
    lfs->cfg.read = ext_flash_xts_read;
    lfs->cfg.prog = ext_flash_xts_prog;
    lfs->cfg.erase = ext_flash_xts_erase;
    return 0;
}

static int ext_flash_xts_read(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, void *buffer, lfs_size_t size){
    const struct flash_area *fa = c->context;
    size_t offset = block * c->block_size + off;

    int rc = flash_area_read(fa, offset, buffer, size);
    // TODO: AES-XTS decryption
    return errno_to_lfs(rc);
}

static int ext_flash_xts_prog(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, const void *buffer, lfs_size_t size){
    const struct flash_area *fa = c->context;
    size_t offset = block * c->block_size + off;

    // TODO: AES-XTS encryption
    int rc = flash_area_write(fa, offset, buffer, size);
    return errno_to_lfs(rc);
}

static int ext_flash_xts_erase(const struct lfs_config *c, lfs_block_t block){
    const struct flash_area *fa = c->context;
    size_t offset = block * c->block_size;

    int rc = flash_area_erase(fa, offset, c->block_size);

    return errno_to_lfs(rc);
}

