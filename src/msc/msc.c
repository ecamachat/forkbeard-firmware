/****************************************************************************
 * Movano Secure Connection
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <zephyr.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/byteorder.h>
#include <random/rand32.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/gatt.h>
#include <logging/log.h>
LOG_MODULE_REGISTER(msc);

#include "sec.h"
#include "msc.h"

#define NR_ALLOWED_MISSING_RX_FRAMES    5
#define BLE_FRAME_HEADER_LEN            2
#define BLE_FRAME_TAG_LEN               4
#define BLE_CRYPTO_SEQ_LEN              4

/*
 *  Movano Secure Connection Crypto
 */
typedef struct _msc_cipher_t {
    bool is_inited;
    movano_hkdf_ctx_t hkdf_ctx;
    mbedtls_gcm_context gcm_ctx;
    uint32_t seqno;
    uint8_t key[CIPHER_KEY_LEN];
    uint8_t iv[CIPHER_IV_LEN];
} msc_cipher_t;

static bool msc_cipher_init(msc_cipher_t *ctx, const sec_certificate_t *cert, const uint8_t *nonce);
static void msc_cipher_free(msc_cipher_t *ctx);
static bool msc_cipher_encrypt_frame(msc_cipher_t *ctx, uint8_t *plain_frame, uint16_t plain_size, uint8_t *encrypted_frame);
static bool msc_cipher_decrypt_frame(msc_cipher_t *ctx, uint8_t *encrypted_frame, uint16_t encrypted_size, uint8_t *decrypted_frame);

static bool msc_cipher_init(msc_cipher_t *ctx, const sec_certificate_t *cert, const uint8_t *nonce) {
    if (ctx->is_inited) {
        msc_cipher_free(ctx);
    }

    if (!movano_secure_derivate_hkdf(cert, nonce, &ctx->hkdf_ctx)) {
        LOG_ERR("msc_cipher_init: ecies failed!");
        return false;
    }

    movano_hkdf_next(&ctx->hkdf_ctx, CIPHER_KEY_LEN, ctx->key);
    LOG_HEXDUMP_DBG(ctx->key, CIPHER_KEY_LEN, "msc_cipher: KEY:");

    ctx->seqno = 1;
    mbedtls_gcm_init(&ctx->gcm_ctx);

    int err;
    err = mbedtls_gcm_setkey(&ctx->gcm_ctx, MBEDTLS_CIPHER_ID_AES, ctx->key, CIPHER_KEY_LEN*8);
    if (err) {
        LOG_ERR("msc_cipher_init: mbedtls_gcm_setkey failed: -0x%04X", (0-err));
        mbedtls_gcm_free(&ctx->gcm_ctx);
        movano_hkdf_free(&ctx->hkdf_ctx);
        return false;
    }
    ctx->is_inited = true;
    return true;
}

static void msc_cipher_free(msc_cipher_t *ctx) {
    mbedtls_gcm_free(&ctx->gcm_ctx);
    movano_hkdf_free(&ctx->hkdf_ctx);
    memset(ctx, 0, sizeof(msc_cipher_t));
}

static bool msc_cipher_encrypt_frame(msc_cipher_t *ctx, uint8_t *plain_frame, uint16_t plain_size, uint8_t *encrypted_frame) {
    int err;
    uint8_t aad[BLE_CRYPTO_SEQ_LEN];
    *((uint32_t*)&aad) = sys_cpu_to_be32(ctx->seqno);
    unsigned ble_frame_payload_len = plain_size - BLE_FRAME_HEADER_LEN;

    movano_hkdf_next(&ctx->hkdf_ctx, CIPHER_IV_LEN, ctx->iv);

    LOG_HEXDUMP_DBG(ctx->key, CIPHER_KEY_LEN, "msc: enc KEY:");
    LOG_HEXDUMP_DBG(ctx->iv, CIPHER_IV_LEN, "msc: enc IV:");
    LOG_HEXDUMP_DBG(aad, BLE_CRYPTO_SEQ_LEN, "msc: enc AAD:");

    err = mbedtls_gcm_crypt_and_tag(
        /* context */ &ctx->gcm_ctx,
        /* mode    */ MBEDTLS_GCM_ENCRYPT,
        /* length  */ ble_frame_payload_len,
        /* iv, len */ ctx->iv, CIPHER_IV_LEN,
        /* aad,len */ aad, BLE_CRYPTO_SEQ_LEN,
        /* input   */ &plain_frame[BLE_FRAME_HEADER_LEN],
        /* output  */ &encrypted_frame[BLE_FRAME_HEADER_LEN],
        /* tag_len */ BLE_FRAME_TAG_LEN,
        /* tag     */ &encrypted_frame[BLE_FRAME_HEADER_LEN + ble_frame_payload_len]
    );
    if (err) {
        LOG_ERR("msc_cipher_encrypt_frame: mbedtls_gcm_crypt_and_tag failed: -0x%04X", (0-err));
        return false;
    }
    LOG_HEXDUMP_DBG(&encrypted_frame[BLE_FRAME_HEADER_LEN], ble_frame_payload_len, "msc: enc Cipher Text:");
    LOG_HEXDUMP_DBG(&encrypted_frame[BLE_FRAME_HEADER_LEN + ble_frame_payload_len], BLE_FRAME_TAG_LEN, "msc: enc TAG:");

    encrypted_frame[0] = plain_frame[0];
    ctx->seqno++;
    return true;
}

static bool msc_cipher_decrypt_frame(msc_cipher_t *ctx, uint8_t *encrypted_frame, uint16_t encrypted_size, uint8_t *decrypted_frame) {
    int err = -1;
    uint8_t aad[BLE_CRYPTO_SEQ_LEN];
    unsigned ble_frame_payload_len = encrypted_size - (BLE_FRAME_HEADER_LEN + BLE_FRAME_TAG_LEN);

    LOG_HEXDUMP_DBG(ctx->key, CIPHER_KEY_LEN, "msc: dec KEY:");
    LOG_HEXDUMP_DBG(ctx->iv, CIPHER_IV_LEN, "msc: dec IV:");
    LOG_HEXDUMP_DBG(&encrypted_frame[BLE_FRAME_HEADER_LEN], ble_frame_payload_len, "msc: dec Cipher Text:");
    LOG_HEXDUMP_DBG(&encrypted_frame[BLE_FRAME_HEADER_LEN + ble_frame_payload_len], BLE_FRAME_TAG_LEN, "msc: dec TAG:");

    uint8_t output[BLE_FRAME_HEADER_LEN+ble_frame_payload_len+BLE_FRAME_TAG_LEN];
    for (unsigned i = 0; i <= NR_ALLOWED_MISSING_RX_FRAMES && err; i++) {  // Allows missing NR_ALLOWED_MISSING_RX_FRAMES BLE frames
        *((uint32_t*)&aad) = sys_cpu_to_be32(ctx->seqno);

        movano_hkdf_next(&ctx->hkdf_ctx, CIPHER_IV_LEN, ctx->iv);

        LOG_HEXDUMP_DBG(aad, BLE_CRYPTO_SEQ_LEN, "msc: dec AAD:");

        err = mbedtls_gcm_auth_decrypt(
            /* context */ &ctx->gcm_ctx,
            /* length  */ ble_frame_payload_len,
            /* iv, len */ ctx->iv, CIPHER_IV_LEN,
            /* aad,len */ aad, BLE_CRYPTO_SEQ_LEN,
            /* tag,len */ &encrypted_frame[BLE_FRAME_HEADER_LEN + ble_frame_payload_len], BLE_FRAME_TAG_LEN,
            /* input   */ &encrypted_frame[BLE_FRAME_HEADER_LEN],
            /* output  */ output
        );
        ctx->seqno++;
    }
    if (err) {
        LOG_ERR("msc_cipher_decrypt_frame: mbedtls_gcm_auth_decrypt failed: -0x%04X", (0-err));
        return false;
    }
    memcpy(&decrypted_frame[BLE_FRAME_HEADER_LEN], output, ble_frame_payload_len);
    LOG_HEXDUMP_DBG(&encrypted_frame[BLE_FRAME_HEADER_LEN], ble_frame_payload_len, "msc: dec Plain Text:");

    decrypted_frame[0] = encrypted_frame[0];
    return true;
}

/*
 * Movano Secure Connection Sesseion
 */
struct _msc_context_t {
    bool is_ready;
    msc_cipher_t tx;
    msc_cipher_t rx;
};

static msc_context_t msc_sessions[CONFIG_BT_MAX_CONN];

int msc_session_setup(msc_context_t *ctx){
    (void)ctx;
    // TODO:
    return 0;
}

int msc_session_tx(msc_context_t *ctx, void *buf, unsigned len){
    (void)ctx;
    (void)buf;
    (void)len;
    // TODO:
    return 0;
}

int msc_session_rx(msc_context_t *ctx, void *buf, unsigned len){
    (void)ctx;
    (void)buf;
    (void)len;
    // TODO:
    return 0;
}

int msc_session_cleanup(msc_context_t *ctx){
    (void)ctx;
    // TODO:
    return 0;
}


#if CONFIG_SHELL
/*
 * Shell commands
 */
#include <shell/shell.h>

static int _cmd_msc_list(const struct shell *shell, size_t argc, char **argv) {
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);
    for (size_t i = 0; i < CONFIG_BT_MAX_CONN; i++) {
        shell_print(shell, "MMS session[%u]: %s", i, log_strdup(msc_sessions[i].is_ready ? "active" : "inactive"));
    }
    return 0;
}

static int _cmd_msc_rx(const struct shell *shell, size_t argc, char **argv) {
    if (argc < 2) {
        shell_print(shell, "msc rx <session> <hex>");
        return 0;
    }
    uint8_t i = strtoul(argv[1], NULL, 10);
    if (i >= CONFIG_BT_MAX_CONN) {
        shell_print(shell, "session out of range <0-%u>", CONFIG_BT_MAX_CONN);
        return 0;
    }
    msc_context_t *ctx = &msc_sessions[i];

    size_t buflen = (strlen(argv[2]) + 1) / 2;
    uint8_t *buf = malloc(buflen);
    if (NULL == buf) {
        shell_print(shell, "out of memory!");
        return 0;
    }

    buflen = hex2bin(argv[2], strlen(argv[2]), buf, buflen);
    if (buflen) {
        shell_print(shell, "Rxing:");
        shell_hexdump(shell, buf, buflen);
        msc_session_rx(ctx, buf, buflen);
    }

    free(buf);
    return 0;
}

static int _cmd_msc_tx(const struct shell *shell, size_t argc, char **argv) {
    if (argc < 2) {
        shell_print(shell, "msc tx <session> <hex>");
        return 0;
    }
    uint8_t i = strtoul(argv[1], NULL, 10);
    if (i >= CONFIG_BT_MAX_CONN) {
        shell_print(shell, "session out of range <0-%u>", CONFIG_BT_MAX_CONN);
        return 0;
    }
    msc_context_t *ctx = &msc_sessions[i];

    size_t buflen = (strlen(argv[2]) + 1) / 2;
    uint8_t *buf = malloc(buflen);
    if (NULL == buf) {
        shell_print(shell, "out of memory!");
        return 0;
    }

    buflen = hex2bin(argv[2], strlen(argv[2]), buf, buflen);
    if (buflen) {
        shell_print(shell, "Txing:");
        shell_hexdump(shell, buf, buflen);
        msc_session_tx(ctx, buf, buflen);
    }

    free(buf);
    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(msc_cmd,
    SHELL_CMD(list, NULL, "List MSC sessions", _cmd_msc_list),
    SHELL_CMD(rx, NULL, "Simulate MSC Rx", _cmd_msc_rx),
    SHELL_CMD(tx, NULL, "Simulate MSC Tx", _cmd_msc_tx),
    SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(msc, &msc_cmd, "Movano Secure Connection commands", NULL);
#endif /* CONFIG_SHELL */
