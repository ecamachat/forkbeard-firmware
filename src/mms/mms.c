/****************************************************************************
 * Movano Message Service
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <zephyr.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/byteorder.h>
#include <random/rand32.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/gatt.h>
#include <logging/log.h>

LOG_MODULE_REGISTER(mms);

#include "btconfig.h"
#include "mms.h"

static mms_service_t mms_service;
static mms_context_t mms_sessions[CONFIG_BT_MAX_CONN];

K_THREAD_STACK_DEFINE(mms_timer_stack, MMS_TIMER_STACK_BYTES);
static void mms_timer_thread_entry(void *p1, void *p2, void *p3);

#if defined(USE_MMS_RX_THREAD) && USE_MMS_RX_THREAD > 0
K_THREAD_STACK_DEFINE(mms_ble_rx_stack, MMS_RX_STACK_BYTES);
static void mms_ble_rx_thread_entry(void *p1, void *p2, void *p3);
#endif
#if defined(USE_MMS_TX_THREAD) && USE_MMS_TX_THREAD > 0
K_THREAD_STACK_DEFINE(mms_ble_tx_stack, MMS_TX_STACK_BYTES);
static void mms_ble_tx_thread_entry(void *p1, void *p2, void *p3);
#endif

static void _rx_session_timer_expired_handler(struct k_timer *timer);
static void _tx_session_timer_expired_handler(struct k_timer *timer);
inline static void process_ble_rx(mms_context_t *ctx, const mms_fragment_t *frame, uint16_t length);
inline static void process_received_message(mms_context_t *ctx, uint8_t *buf, size_t len);

#define TMR_SOURCE_BIT_MASK     0x03
#define TMR_SOURCE_BIT_NBITS    2
#define TMR_SOURCE_BIT_SHIFT    6
#define TMR_SOURCE_VAL_RECV     0
#define TMR_SOURCE_VAL_SEND     1

#define TMR_CTX_INDEX_BIT_MASK  0x3F
#define TMR_CTX_INDEX_BIT_SHIFT 0
#define TMR_CTX_INDEX_BIT_NBITS 6

// static void _dos_timer_expired_handler(struct k_timer *timer)
// {
//     mms_context_t *ctx = (mms_context_t*)k_timer_user_data_get(timer);
//     LOG_INF("[%u]: %s", ctx->index, __func__);

//     uint8_t msg = 0xFF;
//     if (0 == k_msgq_put(&mms_service.mms_timer_msgq, &msg, K_NO_WAIT)) {
//         LOG_INF("[%u]: %s scheduled", ctx->index, __func__);
//     } else {
//         LOG_WRN("[%u]: %s failed to schedule", ctx->index, __func__);
//     }
// }

static void _rx_session_timer_expired_handler(struct k_timer *timer)
{
    mms_context_t *ctx = (mms_context_t*)k_timer_user_data_get(timer);
    LOG_INF("[%u]: %s", ctx->index, __func__);

    uint8_t msg = ctx->index & TMR_CTX_INDEX_BIT_MASK;
    msg |= (TMR_SOURCE_VAL_RECV & TMR_SOURCE_BIT_MASK) << TMR_SOURCE_BIT_SHIFT;
    if (0 == k_msgq_put(&mms_service.mms_timer_msgq, &msg, K_NO_WAIT)) {
        LOG_INF("[%u]: %s scheduled", ctx->index, __func__);
    } else {
        LOG_WRN("[%u]: %s failed to schedule", ctx->index, __func__);
    }
}

static void _tx_session_timer_expired_handler(struct k_timer *timer)
{
    mms_context_t *ctx = (mms_context_t*)k_timer_user_data_get(timer);
    LOG_INF("[%u]: %s", ctx->index, __func__);

    uint8_t msg = ctx->index & TMR_CTX_INDEX_BIT_MASK;
    msg |= (TMR_SOURCE_VAL_SEND & TMR_SOURCE_BIT_MASK) << TMR_SOURCE_BIT_SHIFT;
    if (0 == k_msgq_put(&mms_service.mms_timer_msgq, &msg, K_NO_WAIT)) {
        LOG_INF("[%u]: %s scheduled", ctx->index, __func__);
    } else {
        LOG_WRN("[%u]: %s failed to schedule", ctx->index, __func__);
    }
}

static void _rx_session_session_cleanup(mms_context_t *ctx) {
    LOG_INF("[%u]: %s reset movano message rx session", ctx->index, __func__);
    if (0 == k_mutex_lock(&ctx->rx_session_lock, K_FOREVER)) {
        // cleanup movano message session
        k_timer_stop(&ctx->rx_session_timer);
        memset(ctx->rx_session_buf, 0, sizeof(ctx->rx_session_buf));
        ctx->rx_session_len = 0;
        ctx->rx_session_off = 0;
        ctx->rx_session_state = MMS_SESSION_STATE_CLOSED;
        k_mutex_unlock(&ctx->rx_session_lock);
    } else {
        LOG_ERR("[%u]: %s: failed to take rx_session_lock", ctx->index, __func__);
    }
}
static void _tx_session_session_cleanup(mms_context_t *ctx) {
    LOG_INF("[%u]: %s reset movano message tx session", ctx->index, __func__);
    if (0 == k_mutex_lock(&ctx->tx_session_lock, K_FOREVER)) {
        // cleanup movano message session
        k_timer_stop(&ctx->tx_session_timer);
        memset(ctx->tx_session_buf, 0, sizeof(ctx->tx_session_buf));
        ctx->tx_session_len = 0;
        ctx->tx_session_off = 0;
        ctx->tx_session_state = MMS_SESSION_STATE_CLOSED;
        k_mutex_unlock(&ctx->tx_session_lock);
    } else {
        LOG_ERR("[%u]: %s: failed to take tx_session_lock", ctx->index, __func__);
    }
}

static void mms_reinit_conn(mms_context_t *ctx) {
    _rx_session_session_cleanup(ctx);
    _tx_session_session_cleanup(ctx);
    ctx->conn = NULL;
    ctx->mms_tx_ready = false;
}

void mms_ble_init(const struct bt_gatt_service_static *svc)
{
    mms_service.mms_gatt_svc = svc;
    for (size_t i = 0; i < svc->attr_count; i++) {
        if (0 == bt_uuid_cmp(svc->attrs[i].uuid, BT_UUID_MMS_TX)) {
            mms_service.mms_tx_gatt_attr = &svc->attrs[i];
            LOG_INF("init: Tx attr is attrs[%u]", i);
        }
    }
    if (!mms_service.mms_tx_gatt_attr) {
        LOG_ERR("init: Tx attr not found!");
    }

    for (size_t i = 0; i < CONFIG_BT_MAX_CONN; i++) {
        mms_context_t *ctx = &mms_sessions[i];
        ctx->index = i;
        
#if defined(USE_MMS_RX_THREAD) && USE_MMS_RX_THREAD > 0
        if (k_mutex_init(&ctx->rxq_lock)) {
            LOG_ERR("[%u]: init: rxq_lock fail", i);
        }
#endif
#if defined(USE_MMS_TX_THREAD) && USE_MMS_TX_THREAD > 0
        if (k_mutex_init(&ctx->txq_lock)) {
            LOG_ERR("[%u]: init: txq_lock fail", i);
        }
#endif
        LOG_INF("[%u]: init: rx_session_lock", i);
        if (k_mutex_init(&ctx->rx_session_lock)) {
            LOG_ERR("[%u]: init: rx_session_lock fail", i);
        }
        k_timer_init(&ctx->rx_session_timer, _rx_session_timer_expired_handler, NULL);
        k_timer_user_data_set(&ctx->rx_session_timer, ctx);

        LOG_INF("[%u]: init: tx_session_lock", i);
        if (k_mutex_init(&ctx->tx_session_lock)) {
            LOG_ERR("[%u]: init: tx_session_lock fail", i);
        }
        k_timer_init(&ctx->tx_session_timer, _tx_session_timer_expired_handler, NULL);
        k_timer_user_data_set(&ctx->tx_session_timer, ctx);

        mms_reinit_conn(ctx);
    }

    k_msgq_init(&mms_service.mms_timer_msgq, mms_service.mms_timer_msgq_buf, sizeof(uint8_t), CONFIG_BT_MAX_CONN);
    k_thread_name_set(
                k_thread_create(&mms_service.mms_timer_thread,
                mms_timer_stack, K_THREAD_STACK_SIZEOF(mms_timer_stack),
                mms_timer_thread_entry,
                NULL, NULL, NULL,
                MMS_TIMER_THREAD_PRIO, 0, K_NO_WAIT)
    , "MMS TMR");

#if defined(USE_MMS_RX_THREAD) && USE_MMS_RX_THREAD > 0
    k_msgq_init(&mms_service.mms_ble_rx_msgq, mms_service.mms_ble_rx_msgq_buf, sizeof(uint8_t), CONFIG_BT_MAX_CONN);
    k_thread_name_set(
                k_thread_create(&mms_service.mms_ble_rx_thread,
                mms_ble_rx_stack, K_THREAD_STACK_SIZEOF(mms_ble_rx_stack),
                mms_ble_rx_thread_entry,
                NULL, NULL, NULL,
                MMS_RX_THREAD_PRIO, 0, K_NO_WAIT)
    , "MMS RX");
#endif
#if defined(USE_MMS_TX_THREAD) && USE_MMS_TX_THREAD > 0
    k_msgq_init(&mms_service.mms_ble_tx_msgq, mms_service.mms_ble_tx_msgq_buf, sizeof(uint8_t), CONFIG_BT_MAX_CONN);
    k_thread_name_set(
                k_thread_create(&mms_service.mms_ble_tx_thread,
                mms_ble_tx_stack, K_THREAD_STACK_SIZEOF(mms_ble_tx_stack),
                mms_ble_tx_thread_entry,
                NULL, NULL, NULL,
                MMS_TX_THREAD_PRIO, 0, K_NO_WAIT)
    , "MMS TX");
#endif
}

void mms_ble_connected(struct bt_conn *conn, uint8_t err)
{
    if (err) {
        return;
    }
    mms_context_t *ctx = &mms_sessions[bt_conn_index(conn)];
    LOG_INF("[%u]: %s", ctx->index, __func__);

    mms_reinit_conn(ctx);
    ctx->conn = conn;

    LOG_INF("[%u]: connected", ctx->index);
}

void mms_ble_disconnected(struct bt_conn *conn, uint8_t reason)
{
    mms_context_t *ctx = &mms_sessions[bt_conn_index(conn)];
    LOG_INF("[%u]: %s", ctx->index, __func__);

    mms_reinit_conn(ctx);
    
    LOG_INF("[%u]: disconnected", ctx->index);
}

ssize_t mms_ble_tx(mms_context_t *ctx, const void *buf, uint16_t len)
{
    int err;
#if defined(USE_MMS_TX_THREAD) && USE_MMS_TX_THREAD > 0
    if (0 != k_mutex_lock(&ctx->txq_lock, K_MSEC(250))) {
        LOG_ERR("[%u]: Tx: frame failed to take txq_lock", ctx->index);
        err = -EAGAIN;
    } else {
        ctx->txq_len = len;
        memcpy(ctx->txq_buf, buf, len);
        k_mutex_unlock(&ctx->txq_lock);

        uint8_t msg = ctx->index;
        if (0 != k_msgq_put(&mms_service.mms_ble_tx_msgq, &msg, K_NO_WAIT)) {
            LOG_WRN("[%u]: Tx: frame dropped!", ctx->index);
            err = -EAGAIN;
        }
    }
#else
    // LOG_INF("[%u]: Tx: MTU = %u", ctx->index, bt_gatt_get_mtu(ctx->conn));
    if (!ctx->mms_tx_ready || !mms_service.mms_tx_gatt_attr) {
        LOG_ERR("[%u]: Tx: not ready!", ctx->index);
        err = -EACCES;
    } else {
        LOG_INF("[%u]: Tx: %u bytes", ctx->index, len);
        err = bt_gatt_notify(ctx->conn, mms_service.mms_tx_gatt_attr, buf, len);
        if (err == 0) {
            LOG_HEXDUMP_DBG(buf, len, "BLE Tx:");
            err = len;
        } else {
            err = -EIO;
        }
    }
#endif
    return err;
}

inline static ssize_t mms_ble_rx(mms_context_t *ctx, const void *buf, uint16_t len)
{
    LOG_INF("[%u]: Rx: %u bytes", ctx->index, len);
    LOG_HEXDUMP_DBG(buf, len, "BLE Rx:");

#if defined(USE_MMS_RX_THREAD) && USE_MMS_RX_THREAD > 0

    if (len > sizeof(ctx->rxq_buf)) {
        LOG_WRN("[%u]: Rx: frame too large!", ctx->index);
        return len;
    }

    if (0 != k_mutex_lock(&ctx->rxq_lock, K_MSEC(250))) {
        LOG_ERR("[%u]: Rx: frame failed to take rxq_lock", ctx->index);
        return len;
    }

    ctx->rxq_len = len;
    memcpy(ctx->rxq_buf, buf, len);
    k_mutex_unlock(&ctx->rxq_lock);

    uint8_t msg = ctx->index;
    if (0 == k_msgq_put(&mms_service.mms_ble_rx_msgq, &msg, K_NO_WAIT)) {
        LOG_DBG("[%u]: Rx: frame queued", ctx->index);
    } else {
        LOG_WRN("[%u]: Rx: frame dropped!", ctx->index);
        return len;
    }
#else
    const mms_fragment_t* frame = (const mms_fragment_t*)buf;
    process_ble_rx(ctx, frame, len);
#endif

    return len;
}

ssize_t mms_rx_handler(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags)
{
    const uint8_t *data = (((const uint8_t *)buf) + offset);
    mms_context_t *ctx = &mms_sessions[bt_conn_index(conn)];
    mms_ble_rx(ctx, data, len);
    return len;
}

void mms_tx_ccc_cfg_changed(const struct bt_gatt_attr *attr, uint16_t value)
{
    (void)attr;

    for (unsigned i = 0; i < CONFIG_BT_MAX_CONN; i++) {
        mms_context_t *ctx = &mms_sessions[i];
        if (ctx->conn != NULL) {
            uint16_t ccc;
            bt_gatt_attr_read_ccc(ctx->conn, attr, &ccc, sizeof(uint16_t), 0);
            ctx->mms_tx_ready = (ccc == BT_GATT_CCC_NOTIFY);
            LOG_DBG("[%u]: TxReady: %u", i, ccc);
        }
    }
}

/**
 * @brief process received BLE data
 * 
 * @param ctx 
 * @param buf 
 * @param length 
 */
inline static void process_ble_rx(mms_context_t *ctx, const mms_fragment_t *frame, uint16_t length)
{
    LOG_INF("[%u]: %s: movano message", ctx->index, __func__);
    if (0 != k_mutex_lock(&ctx->rx_session_lock, K_MSEC(250))) {
        LOG_ERR("[%u]: %s: failed to take rx_session_lock", ctx->index, __func__);
        return;
    }

    const uint8_t *content_buf;
    size_t content_len;
    if (frame->data.seqno == 0) {
        // First fragment
        if (ctx->rx_session_state != MMS_SESSION_STATE_CLOSED) {
            LOG_WRN("[%u]: Rx: new movano message delivery while processing previouse one, drop it.", ctx->index);
            k_mutex_unlock(&ctx->rx_session_lock);
            return;
        }
        ctx->rx_session_off = 0;
        ctx->rx_session_len = sys_be32_to_cpu(frame->header.length);
        ctx->rx_session_idx = 1;
        content_len = length - sizeof(mms_fragment_t);
        content_buf = frame->header.payload;
        LOG_INF("[%u]: Rx: 1st movano message fragment: +%u %u/%u bytes", ctx->index, content_len, content_len, ctx->rx_session_len);
    } else {
        // 2nd+ fragment
        if (ctx->rx_session_state != MMS_SESSION_STATE_WAITING_MSG_FRAG) {
            LOG_WRN("[%u]: Rx: 2nd+ movano message: fragment but not waiting for it, drop it.", ctx->index);
            k_mutex_unlock(&ctx->rx_session_lock);
            return;
        }
        if (ctx->rx_session_idx != sys_be16_to_cpu(frame->data.seqno)) {
            LOG_WRN("[%u]: Rx: 2nd+ movano message: invalid fragment index, drop it.", ctx->index);
            k_mutex_unlock(&ctx->rx_session_lock);
            return;
        }
        k_timer_stop(&ctx->rx_session_timer);
        content_len = length - sizeof(mms_fragment_t);
        content_buf = frame->data.payload;
        LOG_INF("[%u]: Rx: %u movano message fragment: +%u %u/%u bytes", ctx->index, sys_be16_to_cpu(frame->data.seqno) + 1, content_len, ctx->rx_session_off + content_len, ctx->rx_session_len);
        ctx->rx_session_idx++;
    }
    if ((content_len + ctx->rx_session_off) > sizeof(ctx->rx_session_buf)) {
        LOG_WRN("[%u]: Rx: movano message: rx_session_len %u, content_len %u > %u", ctx->index, ctx->rx_session_len, content_len, sizeof(ctx->rx_session_buf) - ctx->rx_session_off);
        k_mutex_unlock(&ctx->rx_session_lock);
        return;
    }
    memcpy(&ctx->rx_session_buf[ctx->rx_session_off], content_buf, content_len);
    ctx->rx_session_off += content_len;
    if (ctx->rx_session_off < ctx->rx_session_len) {
        ctx->rx_session_state = MMS_SESSION_STATE_WAITING_MSG_FRAG;
        // start a timer for waiting timeout
        k_timer_start(&ctx->rx_session_timer, K_MSEC(2000), K_NO_WAIT);
        LOG_DBG("[%u]: Rx: movano message: append movano message: %u/%u bytes, waiting next fragment", ctx->index, ctx->rx_session_off, ctx->rx_session_len);
    } else {
        // The end of fragmentation
        if (ctx->rx_session_off != ctx->rx_session_len) {
            LOG_WRN("[%u]: Rx: movano message: declare %u bytes but only got %u bytes in all fragments.", ctx->index, ctx->rx_session_len, ctx->rx_session_off);
            // reply movano message receipt, error
            ctx->rx_session_state = MMS_SESSION_STATE_SENDING_RESPONSE;
            mms_fragment_t receipt;
            memset(&receipt, 0, sizeof(receipt));
            // TODO: fill with NACK
            mms_ble_tx(ctx, &receipt, sizeof(receipt));
        } else {
            // reply movano message receipt, no error
            ctx->rx_session_state = MMS_SESSION_STATE_SENDING_RESPONSE;
            mms_fragment_t receipt;
            memset(&receipt, 0, sizeof(receipt));
            // TODO: fill with ACK            
            mms_ble_tx(ctx, &receipt, sizeof(receipt));

            ctx->rx_session_state = MMS_SESSION_STATE_PROCESSING;
            LOG_INF("[%u]: Rx: movano message: process movano message: %u bytes", ctx->index, ctx->rx_session_len);
            process_received_message(ctx, ctx->rx_session_buf, ctx->rx_session_len);
        }
        // Cleanup movano message session
        _rx_session_session_cleanup(ctx);
    }
    k_mutex_unlock(&ctx->rx_session_lock);

}

inline static void process_received_message(mms_context_t *ctx, uint8_t *buf, size_t len)
{
    // LOG_HEXDUMP_DBG(buf, len, "Movano Message:");

    // TODO: process content
    LOG_HEXDUMP_INF(buf, len, "Received message");
}

void mms_notify_new_data(void *buf, uint16_t len) {
    for (size_t i = 0; i < CONFIG_BT_MAX_CONN; i++) {
        mms_context_t *ctx = &mms_sessions[i];
        // TODO: encode to MMS frame

        // response action result
        mms_ble_tx(ctx, buf, len);
    }
}

static void mms_timer_thread_entry(void *p1, void *p2, void *p3)
{
    for(;;) {
        LOG_INF("Thread: TMR: waiting...");
        uint8_t msg = 0;
        if (0 > k_msgq_get(&mms_service.mms_timer_msgq, &msg, K_FOREVER)) {
            LOG_WRN("Thread: TMR: no message.");
            continue;
        }
        uint8_t source = (msg >> TMR_SOURCE_BIT_SHIFT) & TMR_SOURCE_BIT_MASK;
        uint8_t index = msg & TMR_CTX_INDEX_BIT_MASK;
        if (index >= CONFIG_BT_MAX_CONN) {
            LOG_ERR("Thread: TMR: session index out of range: %u.", index);
            continue;
        }
        mms_context_t *ctx = &mms_sessions[index];

        if (source == TMR_SOURCE_VAL_RECV) {
            // Movano Message rx session
            _rx_session_session_cleanup(ctx);
            LOG_INF("Thread: TMR: cleanup movano message rx session.");
        } else if (source == TMR_SOURCE_VAL_SEND) {
            // Movano Message tx session
            _tx_session_session_cleanup(ctx);
            LOG_INF("Thread: TMR: cleanup movano message tx session.");
        } else {
            LOG_ERR("Thread: TMR: unknown source!");
        }
    }
}

#if defined(USE_MMS_RX_THREAD) && USE_MMS_RX_THREAD > 0
static void mms_ble_rx_thread_entry(void *p1, void *p2, void *p3)
{
    for(;;) {
        LOG_INF("Thread: Rx: waiting...");
        uint8_t msg = 0;
        if (0 > k_msgq_get(&mms_service.mms_ble_rx_msgq, &msg, K_FOREVER)) {
            LOG_INF("Thread: Rx: no message.");
            continue;
        }
        if (msg >= CONFIG_BT_MAX_CONN) {
            LOG_ERR("Thread: Rx: session index out of range: %u.", msg);
            continue;
        }

        mms_context_t *ctx = &mms_sessions[msg];
        if (0 == k_mutex_lock(&ctx->rxq_lock, K_MSEC(250))) {
            LOG_DBG("[%u]: Rx: processing rx...", ctx->index);
            // LOG_HEXDUMP_DBG(ctx->rxq_buf, ctx->rxq_len, "Thread: Rx: buf");
            process_ble_rx(ctx, (const mms_fragment_t*)ctx->rxq_buf, ctx->rxq_len);
            ctx->rxq_len = 0;
            k_mutex_unlock(&ctx->rxq_lock);
        } else {
            LOG_ERR("[%u]: Rx: failed to take rxq_lock", ctx->index);
        }
    }
}
#endif

#if defined(USE_MMS_TX_THREAD) && USE_MMS_TX_THREAD > 0
static void mms_ble_tx_thread_entry(void *p1, void *p2, void *p3)
{
    for(;;) {
        LOG_INF("Thread: Tx: waiting...");
        uint8_t msg = 0;
        if (0 > k_msgq_get(&mms_service.mms_ble_tx_msgq, &msg, K_FOREVER)) {
            LOG_INF("Thread: Tx: no message.");
            continue;
        }
        if (msg >= CONFIG_BT_MAX_CONN) {
            LOG_ERR("Thread: Rx: session index out of range: %u.", msg);
            continue;
        }

        mms_context_t *ctx = &mms_sessions[msg];
        // LOG_INF("[%u]: Tx: MTU = %u", ctx->index, bt_gatt_get_mtu(ctx->conn));

        if (0 == k_mutex_lock(&ctx->txq_lock, K_MSEC(250))) {
            LOG_INF("[%u]: Tx: processing tx...", ctx->index);
            if (!ctx->mms_tx_ready || !mms_service.mms_tx_gatt_attr) {
                LOG_ERR("[%u]: Tx: not ready!", ctx->index);
            } else {
                LOG_INF("[%u]: Tx: %u bytes", ctx->index, ctx->txq_len);
                if (bt_gatt_notify(ctx->conn, mms_service.mms_tx_gatt_attr, ctx->txq_buf, ctx->txq_len) < 0) {
                    LOG_ERR("[%u]: Tx: GATT notify failed", ctx->index);
                } else {
                    LOG_HEXDUMP_DBG(ctx->txq_buf, ctx->txq_len, "BLE Tx:");
                }
            }
            ctx->txq_len = 0;
            k_mutex_unlock(&ctx->txq_lock);
        } else {
            LOG_ERR("[%u]: Tx: failed to take txq_lock", ctx->index);
        }
    }
}
#endif

#if CONFIG_SHELL
/*
 * Shell commands
 */
#include <shell/shell.h>

static int _cmd_mms_list(const struct shell *shell, size_t argc, char **argv) {
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);
    for (size_t i = 0; i < CONFIG_BT_MAX_CONN; i++) {
        shell_print(shell, "MMS session[%u]: %s", i, log_strdup(mms_sessions[i].mms_tx_ready ? "active" : "inactive"));
    }
    return 0;
}

static int _cmd_mms_rx(const struct shell *shell, size_t argc, char **argv) {
    if (argc < 2) {
        shell_print(shell, "mms rx <session> <hex>");
        return 0;
    }
    uint8_t i = strtoul(argv[1], NULL, 10);
    if (i >= CONFIG_BT_MAX_CONN) {
        shell_print(shell, "session out of range <0-%u>", CONFIG_BT_MAX_CONN);
        return 0;
    }
    mms_context_t *ctx = &mms_sessions[i];

    size_t buflen = (strlen(argv[2]) + 1) / 2;
    uint8_t *buf = malloc(buflen);
    if (NULL == buf) {
        shell_print(shell, "out of memory!");
        return 0;
    }

    buflen = hex2bin(argv[2], strlen(argv[2]), buf, buflen);
    if (buflen) {
        shell_print(shell, "Rxing:");
        shell_hexdump(shell, buf, buflen);
        mms_ble_rx(ctx, buf, buflen);
    }

    free(buf);
    return 0;
}

static int _cmd_mms_tx(const struct shell *shell, size_t argc, char **argv) {
    if (argc < 2) {
        shell_print(shell, "mms tx <session> <hex>");
        return 0;
    }
    uint8_t i = strtoul(argv[1], NULL, 10);
    if (i >= CONFIG_BT_MAX_CONN) {
        shell_print(shell, "session out of range <0-%u>", CONFIG_BT_MAX_CONN);
        return 0;
    }
    mms_context_t *ctx = &mms_sessions[i];

    size_t buflen = (strlen(argv[2]) + 1) / 2;
    uint8_t *buf = malloc(buflen);
    if (NULL == buf) {
        shell_print(shell, "out of memory!");
        return 0;
    }

    buflen = hex2bin(argv[2], strlen(argv[2]), buf, buflen);
    if (buflen) {
        shell_print(shell, "Txing:");
        shell_hexdump(shell, buf, buflen);
        mms_ble_tx(ctx, buf, buflen);
    }

    free(buf);
    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(mms_cmd,
    SHELL_CMD(list, NULL, "List MMS sessions", _cmd_mms_list),
    SHELL_CMD(rx, NULL, "Simulate BLE Rx", _cmd_mms_rx),
    SHELL_CMD(tx, NULL, "Simulate BLE Tx", _cmd_mms_tx),
    SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(mms, &mms_cmd, "Movano Message Service commands", NULL);
#endif /* CONFIG_SHELL */
