/****************************************************************************
 * Movano Secure Message
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <zephyr.h>

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/byteorder.h>
#include <mbedtls/gcm.h>

#include <random/rand32.h>
#include <logging/log.h>
LOG_MODULE_REGISTER(msm);

#include <psa/crypto.h>
#include "msm.h"
#include "sec.h"

/*
 * Movano Secure Message
 */

bool movano_secure_message_encrypt(uint8_t *msgbuf, size_t length) {
    msm_header_t *header = (msm_header_t*)msgbuf;
    uint32_t payload_length = sys_be32_to_cpu(header->length);

    if (header->version != SECURE_MESSAGE_VERSION_1) {
        LOG_WRN("secure message encrypt: unsupported version");
        return false;
    }
    if (header->crypto != SECURE_MESSAGE_CRYPTO_AES_GCM_256) {
        LOG_WRN("secure message encrypt: unsupported crypto");
        return false;
    }


    // Fill up nonce
    psa_status_t status = psa_generate_random(header->nonce, sizeof(header->nonce));
    if (status != PSA_SUCCESS) {
        LOG_ERR("secure message encrypt: failed to generate nonce!");
        return false;
    }


    sec_certificate_t backend_cert;
    uint8_t key[CIPHER_KEY_LEN];
    uint8_t iv[CIPHER_IV_LEN];
    movano_secure_certificate_get(0, &backend_cert);
    movano_secure_derivate_key_iv(&backend_cert, header->nonce, key, iv);
    LOG_HEXDUMP_DBG(key, CIPHER_KEY_LEN, "encrypt: secure message: KEY:");
    LOG_HEXDUMP_DBG(iv, CIPHER_IV_LEN, "encrypt: secure message: IV:");

    mbedtls_gcm_context gcm_ctx;
    mbedtls_gcm_init(&gcm_ctx);
    int err;
    err = mbedtls_gcm_setkey(&gcm_ctx, MBEDTLS_CIPHER_ID_AES, key, CIPHER_KEY_LEN*8);
    if (err) {
        LOG_ERR("secure message encrypt: mbedtls_gcm_setkey failed: -0x%04X", (0-err));
        mbedtls_gcm_free(&gcm_ctx);
        return false;
    }
    err = mbedtls_gcm_crypt_and_tag(
        /* context */ &gcm_ctx,
        /* mode    */ MBEDTLS_GCM_ENCRYPT,
        /* length  */ payload_length,
        /* iv, len */ iv, CIPHER_IV_LEN,
        /* aad,len */ msgbuf, sizeof(msm_header_t),
        /* input   */ header->payload,
        /* output  */ header->payload,
        /* tag_len */ sizeof(msm_footer_t),
        /* tag     */ &msgbuf[sizeof(msm_header_t) + payload_length]
    );
    mbedtls_gcm_free(&gcm_ctx);
    if (err) {
        LOG_ERR("secure message encrypt: failed");
        return false;
    }
    return true;
}

bool movano_secure_message_decrypt(uint8_t *msgbuf, size_t length) {
    msm_header_t *header = (msm_header_t*)msgbuf;
    uint32_t payload_length = sys_be32_to_cpu(header->length);

    if (length < (sizeof(msm_header_t) + sizeof(msm_footer_t))) {
        LOG_WRN("secure message decrypt: message too short");
        return false;
    }
    if (header->version != SECURE_MESSAGE_VERSION_1) {
        LOG_WRN("secure message decrypt: unsupported version");
        return false;
    }
    if (header->crypto != SECURE_MESSAGE_CRYPTO_AES_GCM_256) {
        LOG_WRN("secure message decrypt: unsupported crypto");
        return false;
    }
    if (length < (sizeof(msm_header_t) + sizeof(msm_footer_t) + payload_length)) {
        LOG_WRN("secure message decrypt: invalid payload length");
        return false;
    }

    sec_certificate_t backend_cert;
    uint8_t key[CIPHER_KEY_LEN];
    uint8_t iv[CIPHER_IV_LEN];
    movano_secure_certificate_get(0, &backend_cert);
    movano_secure_derivate_key_iv(&backend_cert, header->nonce, key, iv);
    LOG_HEXDUMP_DBG(header->nonce, sizeof(header->nonce), "decrypt: secure message: RND:");
    LOG_HEXDUMP_DBG(key, CIPHER_KEY_LEN, "decrypt: secure message: KEY:");
    LOG_HEXDUMP_DBG(iv, CIPHER_IV_LEN, "decrypt: secure message: IV:");
    LOG_HEXDUMP_DBG(msgbuf, sizeof(msm_header_t), "decrypt: secure message: AAD:");
    LOG_HEXDUMP_DBG(header->payload, payload_length, "decrypt: secure message: Cipher Text:");
    LOG_HEXDUMP_DBG(&msgbuf[sizeof(msm_header_t) + payload_length], sizeof(msm_footer_t), "decrypt: secure message: TAG:");

    mbedtls_gcm_context gcm_ctx;
    mbedtls_gcm_init(&gcm_ctx);
    int err;
    err = mbedtls_gcm_setkey(&gcm_ctx, MBEDTLS_CIPHER_ID_AES, key, CIPHER_KEY_LEN*8);
    if (err) {
        LOG_ERR("secure message decrypt: mbedtls_gcm_setkey failed: -0x%04X", (0-err));
        mbedtls_gcm_free(&gcm_ctx);
        return false;
    }
    err = mbedtls_gcm_auth_decrypt(
        /* context */ &gcm_ctx,
        /* length  */ payload_length,
        /* iv, len */ iv, CIPHER_IV_LEN,
        /* aad,len */ msgbuf, sizeof(msm_header_t),
        /* tag,len */ &msgbuf[sizeof(msm_header_t) + payload_length], sizeof(msm_footer_t),
        /* input   */ header->payload,
        /* output  */ header->payload
    );
    mbedtls_gcm_free(&gcm_ctx);
    if (err) {
        LOG_ERR("secure message decrypt: corrupted, err = -0x%X", 0 - err);
        return false;
    }
    // LOG_HEXDUMP_DBG(header->payload, payload_length, "decrypt: secure message: Plain Text:");
    return true;
}

#if CONFIG_SHELL
/*
 * Shell commands
 */
#include <shell/shell.h>

static int _cmd_msm_decode(const struct shell *shell, size_t argc, char **argv) {
    (void)shell;
    (void)argc;
    (void)argv;
    return 0;
}

static int _cmd_msm_encode(const struct shell *shell, size_t argc, char **argv) {
    (void)shell;
    (void)argc;
    (void)argv;
    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(msm_cmd,
    SHELL_CMD(decode, NULL, "Decode MSM file", _cmd_msm_decode),
    SHELL_CMD(encode, NULL, "Eecode MSM file", _cmd_msm_encode),
    SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(msm, &msm_cmd, "Movano Secure Message commands", NULL);
#endif
