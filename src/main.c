/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <zephyr.h>
#include <sys/reboot.h>
#include <sys/printk.h>

#include "shell_movano.h"
#include "btconfig.h"
#include "watchdog.h"
#include "ext_flash.h"
#include "sec.h"
#include "mms.h"
#include "msc.h"
#include "msm.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(main);

extern const char *FW_VERSION;

void main(void)
{
    LOG_INF("Movano version %s", FW_VERSION);

    /* Intialize Watchdog */
    if (wdog_init()) {
        LOG_ERR("Error: Unable to initialize hardware watchdog");
        /* No reboot for now */
    }

    /* Initialize Shell */
    shell_init_movano();

    /* Initialize Security */
    movano_sec_init();

    /* Initialize Flash */
    if (ext_flash_init()) {
        LOG_ERR("Error: Unable to initialize external flash");
        /* No reboot for now */
    }

    /* Initialize Movano Message Service (MMS) / Bluetooth */
    if (bt_init()) {
        LOG_ERR("Error: Unable to configure Bluetooth");
        sys_reboot(SYS_REBOOT_COLD);
    }

}
