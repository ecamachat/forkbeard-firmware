/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <sys/reboot.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <logging/log.h>

#include "btconfig.h"
#include "movano.h"
#include "mms.h"

LOG_MODULE_REGISTER(bluetooth);

#define DEVICE_NAME         CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN     (sizeof(DEVICE_NAME) - 1)

/* Get the device name, Movano prefix and BLE MAC (for now) */
static int mov_get_bt_name_str(char *id, size_t len)
{
    bt_addr_le_t addrs[CONFIG_BT_ID_MAX];
    size_t count = CONFIG_BT_ID_MAX;

    if (len < CONFIG_BT_DEVICE_NAME_MAX-1) {
        return -1;
    }

    bt_id_get(addrs, &count);

    if (count > 0) {
        return snprintk(id, len-1, "%s%02X%02X%02X%02X%02X%02X", MOVANO_ID_PREFIX,
            addrs[0].a.val[5], addrs[0].a.val[4], addrs[0].a.val[3],
            addrs[0].a.val[2], addrs[0].a.val[1], addrs[0].a.val[0]);
    }
    else {
        return -2;
    }
}


/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_MMS_SVC_VAL),
};


/**
 * @brief Movano Message Service
 * 
 */
#include "mms.h"

/* Movano Message Service Declaration */
BT_GATT_SERVICE_DEFINE(mms_svc,
    BT_GATT_PRIMARY_SERVICE(BT_UUID_MMS_SVC),
        BT_GATT_CHARACTERISTIC(BT_UUID_MMS_RX,
                    BT_GATT_CHRC_WRITE,
                    BT_GATT_PERM_WRITE,
                    NULL, mms_rx_handler, NULL),
        BT_GATT_CHARACTERISTIC(BT_UUID_MMS_TX,
                    BT_GATT_CHRC_NOTIFY,
                    BT_GATT_PERM_NONE,
                    NULL, NULL, NULL),
        BT_GATT_CCC(mms_tx_ccc_cfg_changed, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
);

/* Bluetooth initilization */
static void connected(struct bt_conn *conn, uint8_t err)
{
    (void)conn;
    if (err) {
        LOG_ERR("Connection failed (err %u)\n", err);
        return;
    }
    LOG_INF("Connected");
    
    mms_ble_connected(conn, err);
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
    LOG_INF("Disconnected (reason %u)", reason);

    mms_ble_disconnected(conn, reason);
}

static bool le_param_req(struct bt_conn *conn, struct bt_le_conn_param *param)
{
    (void)conn;
    LOG_INF("le_param_req: interval [%u, %u], latency %u, timeout, %u", param->interval_min, param->interval_max, param->latency, param->timeout);
    return true;
}

static void le_param_updated(struct bt_conn *conn, uint16_t interval, uint16_t latency, uint16_t timeout)
{
    (void)conn;
    LOG_INF("le_param_updated: interval %u, latency %u, timeout, %u", interval, latency, timeout);
}

                 
#if defined(CONFIG_BT_USER_PHY_UPDATE)
static void le_phy_updated(struct bt_conn *conn, struct bt_conn_le_phy_info *param)
{
    (void)conn;
    LOG_INF("le_phy_updated: Rx: 0x%02X, Tx 0x%02X", param->rx_phy, param->tx_phy);
}
#endif

#if defined(CONFIG_BT_USER_DATA_LEN_UPDATE)
static void le_data_len_updated(struct bt_conn *conn, struct bt_conn_le_data_len_info *info)
{
    (void)conn;
    LOG_INF("le_data_len_updated: Rx: (len %u, time %u); Tx: (len %u, time %u)", info->rx_max_len, info->rx_max_time, info->tx_max_len, info->tx_max_time);
}
#endif

static struct bt_conn_cb conn_callbacks = {
    .connected           = connected,
    .disconnected        = disconnected,
    .le_param_req        = le_param_req,
    .le_param_updated    = le_param_updated,
#if defined(CONFIG_BT_USER_PHY_UPDATE)
    .le_phy_updated      = le_phy_updated,
#endif
#if defined(CONFIG_BT_USER_DATA_LEN_UPDATE)
    .le_data_len_updated = le_data_len_updated,
#endif
};

int bt_init(void)
{
    int err;

    bt_conn_cb_register(&conn_callbacks);
    err = bt_enable(NULL);
    if (err) {
        LOG_ERR("Bluetooth initialization failed (err %d)", err);
        return -1;
    }

    /* Initialize MMS Service */
    mms_ble_init(&mms_svc);
    
    /* Start BT Advertising */
    if (bt_start_advertising()) {
        LOG_ERR("Error: Unable to start BT advertising");
        sys_reboot(SYS_REBOOT_COLD);
    }

    LOG_INF("Bluetooth initialized");
    return 0;
}

int bt_start_advertising(void)
{
    int err;
    char bt_device_name[CONFIG_BT_DEVICE_NAME_MAX];

    if (mov_get_bt_name_str(bt_device_name, CONFIG_BT_DEVICE_NAME_MAX) <= 0) {
        LOG_ERR("Unable to set bluetooth device name");
        return -1;
    }

    const struct bt_data ad[] = {
        BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
        BT_DATA(BT_DATA_NAME_COMPLETE, bt_device_name, strlen(bt_device_name))
    };

    err = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad),
              sd, ARRAY_SIZE(sd));
    if (err) {
        LOG_ERR("Advertising failed to start (err %d)", err);
        return -1;
    }
    LOG_INF("Advertising successfully started");
    return 0;
}
