/****************************************************************************
 * Copyright (c) 2022 Movano Inc.
 ****************************************************************************/

#include <zephyr.h>
#include <drivers/watchdog.h>

#include "watchdog.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(watchdog);

/*
 * If the devicetree has a watchdog node, get its label property.
 */
#ifdef WDT_NODE
#define WDT_DEV_NAME DT_LABEL(WDT_NODE)
#else
#define WDT_DEV_NAME ""
#error "Unsupported SoC and no watchdog0 alias in zephyr.dts"
#endif

static bool wdog_feed_enable = true;
/*
 * watchdog device handle and channel id
 */
struct wdt_timer {
    const struct device *wdt;
    int wdt_channel_id;
} telca_wdg_timer;

/** @brief Handle watchdog refresh timer expriration
 */
static void _wdog_refresh_timer_expired_handler(struct k_timer *timer)
{
    struct wdt_timer *wdog_tmr_data = (struct wdt_timer *)timer->user_data;
    if (wdog_feed_enable) {
        wdt_feed(wdog_tmr_data->wdt, wdog_tmr_data->wdt_channel_id);
    }
    k_timer_start(timer, K_SECONDS(WDOG_REFRESH_INTERVAL_SECONDS), K_NO_WAIT);
}
K_TIMER_DEFINE(watchdog_timer, _wdog_refresh_timer_expired_handler, NULL);

/** @brief Initialize the Hardware Watchdog.
 *
 * This function sets up the hardware watchdog registers, then starts
 * the watchdog.  Eventually, watchdog refresh should be moved to a
 * thread (sysmon).
 *
 * @retval 0 If the operation was successful.
 *           Otherwise, a (negative) error code is returned.
 */
int wdog_init(void)
{
    int wdt_channel_id;
	const struct device *wdt;
	struct wdt_timeout_cfg wdt_config;
    int err;

	wdt = device_get_binding(WDT_DEV_NAME);
	if (!wdt) {
		LOG_ERR("Cannot get WDT device\n");
		return -1;
	}

	/* Reset SoC when watchdog timer expires. */
	wdt_config.flags = WDT_FLAG_RESET_SOC;

	/* Expire watchdog after WDOG_TIMEOUT_IN_MS milliseconds. */
	wdt_config.window.min = 0U;
	wdt_config.window.max = WDOG_TIMEOUT_MS;

	/* No watchdog callback. */
	wdt_config.callback = NULL;

	wdt_channel_id = wdt_install_timeout(wdt, &wdt_config);
	if (wdt_channel_id < 0) {
		LOG_ERR("Watchdog install error\n");
		return -1;
	}

	err = wdt_setup(wdt, 0);
	if (err < 0) {
		LOG_ERR("Watchdog setup error\n");
		return -1;
	}

    telca_wdg_timer.wdt = wdt;
    telca_wdg_timer.wdt_channel_id = wdt_channel_id;
    watchdog_timer.user_data = &telca_wdg_timer;
    k_timer_start(&watchdog_timer, K_SECONDS(WDOG_REFRESH_INTERVAL_SECONDS), K_NO_WAIT);
    LOG_INF("Watchdog Initialized");
    return 0;
}

#if CONFIG_SHELL
/*
 * Shell commands
 */
#include <shell/shell.h>
#include <shell/shell_uart.h>

static int _cmd_watchdog_off(const struct shell *shell, size_t argc,
                  char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);
    wdog_feed_enable = false;
    return 0;
}

static int _cmd_watchdog_on(const struct shell *shell, size_t argc,
                  char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);
    wdog_feed_enable = true;
    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(sub_watchdog,
    SHELL_CMD(off, NULL, "Disable Watchdog Refresh",
          _cmd_watchdog_off),
    SHELL_CMD(on, NULL, "Enable Watchdog Refresh",
          _cmd_watchdog_on),
    SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(watchdog, &sub_watchdog, "Watchdog commands", NULL);
#endif
