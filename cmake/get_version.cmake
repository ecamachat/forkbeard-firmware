# Git executable is extracted from parameters.
find_package(Git QUIET)

execute_process(COMMAND ${GIT_EXECUTABLE} describe --tags --match [0-9]*.[0-9]*
    RESULT_VARIABLE git_result OUTPUT_VARIABLE GIT_REPO_VERSION ERROR_VARIABLE git_error)

string(STRIP ${GIT_REPO_VERSION} GIT_REPO_VERSION)
if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    string(CONCAT GIT_REPO_VERSION ${GIT_REPO_VERSION} "+dbg")
endif()
message(STATUS "Project version ${GIT_REPO_VERSION}")

set (VERSION "#ifdef SW_VERSION
const char *FW_VERSION=SW_VERSION;
#else
const char *FW_VERSION=\"${GIT_REPO_VERSION}\";
#endif")

if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/version.c)
    file(READ ${CMAKE_CURRENT_SOURCE_DIR}/version.c SW_VERSION_)
else()
    set(SW_VERSION_ "")
endif()

if (NOT "${VERSION}" STREQUAL "${SW_VERSION_}")
    file(WRITE ${CMAKE_CURRENT_SOURCE_DIR}/version.c "${VERSION}")
endif()
