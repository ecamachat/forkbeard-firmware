# Makefile

TOOLCHAIN_DIR	?= $(abspath $(shell dirname $$(dirname $$(which arm-none-eabi-gcc))))
NRF_SDK_DIR     ?= /Users/eric/Projects/GitHub/nrfconnect/v1.9.1
SW_SIGNING_KEY	?= $(abspath keys/signing-dev.pem)
PYTHON_CMD	    ?= python3

BOARD            = nrf5340dk_nrf5340_cpuapp
TOOLCHAIN_VAR    = gnuarmemb
ZEPHYR_DIR       = $(NRF_SDK_DIR)/zephyr
SHELL_ENV		 = export BOARD=$(BOARD); \
				   export ZEPHYR_TOOLCHAIN_VARIANT=$(TOOLCHAIN_VAR); \
				   export GNUARMEMB_TOOLCHAIN_PATH=$(TOOLCHAIN_DIR); \
	               source $(ZEPHYR_DIR)/zephyr-env.sh;
CMAKE_CMD		 = $(SHELL_ENV) $(addprefix env ,$(CMAKE_ENV)) cmake
IMGTOOL			 = $(NRF_SDK_DIR)/bootloader/mcuboot/scripts/imgtool.py
ASSEMBLE		 = $(NRF_SDK_DIR)/bootloader/mcuboot/scripts/assemble.py
PYOCD			 = pyocd

BL_SLOT_SIZE	 = 0x10000
FW_SLOT_SIZE	 = 0xF0000
FW_HEADER_LEN	 = 0x200
FLASH_ALIGNMENT	 = 8

ifeq (,$(SW_VERSION))
	SW_VERSION		:= $(shell git describe --tags --match '[0-9]*.[0-9]*' | sed  -e 's,-,+,' -e 's,-g.*,,')
else
	CMAKE_ARGS	+= -DEXTRA_CFLAGS=-DSW_VERSION=\\\"$(SW_VERSION)\\\"
endif

# SW configs
CMAKE_ARGS      += -DBOARD=$(BOARD)
CMAKE_ARGS      += -DPM_STATIC_YML_FILE=$(abspath conf/pm_static.yml)
CMAKE_ARGS      += -DCONF_FILE=$(abspath conf/prj.conf)
CMAKE_ARGS		+= -DCONFIG_MCUBOOT_IMAGE_VERSION=\"$(SW_VERSION)\"
#CMAKE_ARGS		+= -DCONFIG_FLASH_LOAD_OFFSET=$(BL_SLOT_SIZE)
CMAKE_ARGS		+= -DCONFIG_FW_INFO_OFFSET=$(FW_HEADER_LEN)

# CPUNET
CMAKE_ARGS      += -Dhci_rpmsg_CONFIG_SB_SIGNING_KEY_FILE=\\\"$(SW_SIGNING_KEY)\\\"
CMAKE_ARGS      += -Dhci_rpmsg_CONFIG_FW_INFO_FIRMWARE_VERSION=1
CMAKE_ARGS      += -Dhci_rpmsg_CONFIG_SB_PUBLIC_KEY_FILES=\\\"\\\"

CMAKE_GEN = 'Unix Makefiles'
BUILD_CMD = $(MAKE)
ifeq (1,$(V))
	CMAKE_ARGS  += -DCMAKE_VERBOSE_MAKEFILE=ON
	CMAKE_ARGS  += -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
else
	CMAKE_ARGS  += -DCMAKE_VERBOSE_MAKEFILE=OFF
    # Prefer Samurai/Ninja then Unix Makefiles
	ifneq (0,$(PREFER_NINJA))
		ifneq (,$(shell which samu))
			CMAKE_GEN = 'Ninja'
			BUILD_CMD = $(shell which samu)
		else
			ifneq (,$(shell which ninja))
				CMAKE_GEN = 'Ninja'
				BUILD_CMD = $(shell which ninja)
			endif
		endif
	endif
endif

.SUFFIXES:            # Delete the default suffixes

all: build

config: build/.configured

build: build/.built $(shell find . -type f -name '*.c' -o -type f -name '*.h')

sign: build/zephyr/app_signed.hex

reconfig:
	$(MAKE) clean config

rebuild:
	$(MAKE) clean build

build/.created:
	@[ -d build ] || mkdir build
	@touch build/.created

build/.configured: build/.build-tools Makefile CMakeLists.txt $(wildcard conf/*) $(wildcard keys/*)
	$(addprefix env ,$(CMAKE_ENV)) $(CMAKE_CMD) -B build -G $(CMAKE_GEN) $(CMAKE_ARGS)
	@touch build/.configured

build/.built: build/.build-tools build/.configured
	$(addprefix env ,$(BUILD_ENV)) $(BUILD_CMD) -C build all $(addprefix VERBOSE=,$(V))
	@touch build/.built

clean:
	@[ -d build ] && rm -rf build || true

show-memory-map:
	@$(addprefix env ,$(BUILD_ENV)) $(BUILD_CMD) -C build partition_manager_report
	@#if [ -n "$(wildcard build/partitions*.yml)" ]; then $(PYTHON_CMD) $(NRF_SDK_DIR)/nrf/scripts/partition_manager_report.py --input $(wildcard build/partitions*.yml); else echo "No partition conf file."; false; fi

reset:
	nrfjprog -f NRF53 --reset

flash: flash-app

flash-erase:
	nrfjprog -f NRF53 --recover

flash-app: build/.flash-tools build/.built sign
	nrfjprog -f NRF53 --verify --reset --program build/zephyr/app_signed.hex --sectorerase

build/zephyr/app_signed.hex: build/.flash-tools build/.built
	$(IMGTOOL) sign \
		--key $(SW_SIGNING_KEY) \
		--header-size $(FW_HEADER_LEN) \
		--align $(FLASH_ALIGNMENT) \
		--version $(SW_VERSION) \
		--slot-size $(FW_SLOT_SIZE) \
		--pad-header \
		build/zephyr/zephyr.hex \
		build/zephyr/app_signed.hex

check-build-tools: build/.build-tools

build/.build-tools: build/.created
	@if [ $$(sh -c 'cmake --help >/dev/null 2>/dev/null; echo $$?') -eq 127 ]; then echo "cmake not found!"; false; else true; fi
	@if [ $$(sh -c '$(BUILD_CMD) --help >/dev/null 2>/dev/null; echo $$?') -eq 127 ]; then echo "$(BUILD_CMD) not found!"; false; else true; fi
	@touch build/.build-tools

check-flash-tools: build/.flash-tools
	@if [ $$(sh -c 'nrfjprog --help >/dev/null 2>/dev/null; echo $$?') -eq 127 ]; then echo "nrfjprog not found!"; false; else true; fi

build/.flash-tools: build/.created
	@if [ $$(sh -c 'nrfjprog --help >/dev/null 2>/dev/null; echo $$?') -eq 127 ]; then echo "nrfjprog not found!"; false; else true; fi
	@touch build/.flash-tools

gen-signing-keys:
	@if ! [ -f $(SW_SIGNING_KEY) ]; then openssl ecparam -name prime256v1 -genkey -noout -out $(SW_SIGNING_KEY); fi

menuconfig:
	$(addprefix env ,$(BUILD_ENV)) $(BUILD_CMD) -C build menuconfig

help:
	@echo "make <clean|config|build|rebuild|sign|flash-app|flash-erase|show-memory-map|gen-signing-keys|menuconfig>"

print-%:
	@echo $* = $($*)

.PHONY: all build rebuild check-build-tools check-flash-tools clean config reconfig flash flash-erase flash-app gen-signing-keys menuconfig help reset show-memory-map sign
